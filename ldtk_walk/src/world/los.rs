use bevy::{math::vec2, prelude::*};
use bevy_ecs_tilemap::tiles::TilePos;
use ok_math::Quad;

use super::TILE_WIDTH;

pub(crate) fn provide_los_obstacles(
    q: Query<(&TilePos, &LosOpaqueTile)>,
    mut obstacles: ResMut<los::LosObstacles>,
) {
    let lines = q
        .iter()
        .map(|(tile_pos, _)| {
            let tile_pos = vec2(tile_pos.x as f32, tile_pos.y as f32) * TILE_WIDTH;

            Quad::with_start_size(tile_pos, Vec2::splat(TILE_WIDTH))
        })
        .flat_map(|q| q.line_segments());

    *obstacles = los::LosObstacles::with_lines(lines);
}

#[derive(Debug, Component)]
pub struct LosOpaqueTile;

impl Default for LosOpaqueTile {
    fn default() -> Self {
        Self
    }
}
