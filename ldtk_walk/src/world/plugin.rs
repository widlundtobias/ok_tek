use bevy::prelude::Update;
use bevy_ecs_ldtk::{prelude::LdtkIntCellAppExt, LdtkPlugin, LevelSelection};

use super::{los::provide_los_obstacles, GroundBundle, WallBundle};

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins((
            LdtkPlugin,
            los::Plugin {
                config: los::LosConfig {
                    shadow_opacity: 0.8,
                    composite_camera_scale: 0.25,
                },
            },
        ))
        .insert_resource(LevelSelection::Index(0))
        .register_ldtk_int_cell::<WallBundle>(1)
        .register_ldtk_int_cell::<GroundBundle>(2)
        .add_systems(Update, provide_los_obstacles);
    }
}
