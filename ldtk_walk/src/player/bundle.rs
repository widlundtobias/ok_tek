use bevy::{math::vec2, prelude::*};
use bevy_rapier2d::prelude::*;

use crate::movement;

use super::Player;

#[derive(Bundle)]
pub struct PlayerBundle {
    pub move_intention: movement::MoveIntention,
    pub collider: Collider,
    pub rigid_body: RigidBody,
    pub velocity: Velocity,
    pub rotation_constraints: LockedAxes,
    pub sprite_bundle: SpriteBundle,
    pub player: Player,
}

impl PlayerBundle {
    pub fn new(position: Vec2, asset_server: &AssetServer) -> Self {
        let half_size = vec2(3.0, 3.0);

        Self {
            move_intention: movement::MoveIntention::new_empty(),
            collider: Collider::cuboid(half_size.x, half_size.y),
            rigid_body: RigidBody::Dynamic,
            rotation_constraints: LockedAxes::ROTATION_LOCKED,
            sprite_bundle: SpriteBundle {
                texture: asset_server.load("player.png"),
                transform: Transform::default().with_translation(position.extend(100.0)),
                ..Default::default()
            },
            player: Player {},
            velocity: Default::default(),
        }
    }
}
