use bevy::prelude::Update;

use super::process_player_input;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_systems(Update, process_player_input);
    }
}
