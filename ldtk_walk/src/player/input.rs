use bevy::{math::vec2, prelude::*};

use crate::movement;

use super::Player;

pub fn process_player_input(
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<(&mut movement::MoveIntention, &Player)>,
) {
    let as_direction = |pressed, direction| if pressed { direction } else { Vec2::ZERO };

    let left = keyboard_input.pressed(KeyCode::A);
    let right = keyboard_input.pressed(KeyCode::D);
    let up = keyboard_input.pressed(KeyCode::W);
    let down = keyboard_input.pressed(KeyCode::S);

    let direction = as_direction(left, vec2(-1.0, 0.0))
        + as_direction(right, vec2(1.0, 0.0))
        + as_direction(up, vec2(0.0, 1.0))
        + as_direction(down, vec2(0.0, -1.0));

    let direction = if direction.length() == 0.0 {
        None
    } else {
        Some(direction)
    };

    for (mut move_intention, _) in query.iter_mut() {
        *move_intention = movement::MoveIntention::new(direction).unwrap();
    }
}
