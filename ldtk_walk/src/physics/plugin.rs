use bevy::prelude::{ResMut, Startup, Vec2};
use bevy_rapier2d::prelude::{NoUserData, RapierConfiguration, RapierPhysicsPlugin};

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(100.0)) // Add the plugin
            .add_systems(Startup, setup_physics);
    }
}

pub fn setup_physics(mut rapier_config: ResMut<RapierConfiguration>) {
    rapier_config.gravity = Vec2::ZERO;

    /*
    /*
     * Ground
     */
    let ground_size = 500.0;
    let ground_height = 1.0;

    commands.spawn((
        TransformBundle::from(Transform::from_xyz(0.0, -ground_height, 0.0)),
        Collider::cuboid(ground_size, ground_height),
    ));

    /*
     * Create the cubes
     */
    let num = 4;
    let rad = 2.0;

    let shift = rad * 4.0 + rad;
    let centerx = shift * (num / 2) as f32;
    let centery = shift / 2.0;

    let mut offset = -(num as f32) * (rad * 2.0 + rad) * 0.5;

    for j in 0usize..20 {
        for i in 0..num {
            let x = i as f32 * shift * 5.0 - centerx + offset;
            let y = j as f32 * (shift * 5.0) + centery + 3.0;

            commands
                .spawn((
                    TransformBundle::from(Transform::from_xyz(x, y, 0.0)),
                    RigidBody::Dynamic,
                ))
                .with_children(|children| {
                    children.spawn(Collider::cuboid(rad * 10.0, rad));
                    children.spawn((
                        TransformBundle::from(Transform::from_xyz(rad * 10.0, rad * 10.0, 0.0)),
                        Collider::cuboid(rad, rad * 10.0),
                    ));
                    children.spawn((
                        TransformBundle::from(Transform::from_xyz(-rad * 10.0, rad * 10.0, 0.0)),
                        Collider::cuboid(rad, rad * 10.0),
                    ));
                });
        }

        offset -= 0.05 * rad * (num as f32 - 1.0);
    }*/
}
