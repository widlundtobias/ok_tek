use bevy::prelude::*;
use bevy_ecs_ldtk::IntGridCell;
use bevy_rapier2d::prelude::*;
//use heron::{CollisionShape, RigidBody, RotationConstraints};

impl From<IntGridCell> for WallColliderBundle {
    fn from(_: IntGridCell) -> Self {
        let half_tile_size = 4.0;

        Self {
            collider: Collider::cuboid(half_tile_size, half_tile_size),
            rigid_body: RigidBody::Fixed,
            rotation_constraints: LockedAxes::ROTATION_LOCKED,
        }
    }
}

#[derive(Default, Bundle)]
pub struct WallColliderBundle {
    pub collider: Collider,
    pub rigid_body: RigidBody,
    pub rotation_constraints: LockedAxes,
}
