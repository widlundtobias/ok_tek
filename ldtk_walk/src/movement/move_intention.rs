use bevy::prelude::*;
use bevy_rapier2d::prelude::Velocity;
use ok_math::{safe_normalize, ZeroVecError};

use crate::player;

#[derive(Debug, PartialEq, Clone, Copy, Component)]
pub struct MoveIntention {
    direction: Option<Vec2>,
}

impl Eq for MoveIntention {}

impl MoveIntention {
    pub fn new(direction: Option<Vec2>) -> Result<Self, ZeroVecError> {
        let direction = match direction {
            Some(v) => Some(safe_normalize(v)?),
            None => None,
        };

        Ok(MoveIntention { direction })
    }

    pub fn new_empty() -> Self {
        Self { direction: None }
    }

    pub fn direction(&self) -> &Option<Vec2> {
        &self.direction
    }
}

pub fn update_movement(mut query: Query<(&mut Velocity, &MoveIntention)>) {
    for (mut velocity, move_intention) in query.iter_mut() {
        let direction = move_intention.direction().unwrap_or_default();
        let vel = direction * player::PLAYER_SPEED; // TODO: break out to a proper stats struct

        velocity.linvel = vel;
    }
}
