use bevy::prelude::Update;

use super::update_movement;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_systems(Update, update_movement);
    }
}
