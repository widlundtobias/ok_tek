use bevy::{
    log,
    math::{vec2, vec3},
    prelude::*,
};
use bevy_ecs_ldtk::prelude::*;

mod movement;
mod physics;
mod player;
mod world;

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins.set(ImagePlugin::default_nearest()),
            movement::Plugin,
            player::Plugin,
            world::Plugin,
            physics::Plugin,
        ))
        .insert_resource(ClearColor(Color::rgb(0.1, 0.1, 0.1)))
        .add_systems(Startup, setup)
        .run();
}

const WIDTH: f32 = 1366.0;
const HEIGHT: f32 = 768.0;

#[derive(Component, Default)]
pub struct BlendPassMesh;

fn setup(mut windows: Query<&mut Window>, mut commands: Commands, asset_server: Res<AssetServer>) {
    let mut window = windows.single_mut();
    window.resolution.set(WIDTH, HEIGHT);

    let id = commands
        .spawn(LdtkWorldBundle {
            ldtk_handle: asset_server.load("my_project.ldtk"),
            transform: Transform::default().with_translation(vec3(0.0, 0.0, 0.0)),
            visibility: Visibility::Visible,
            ..Default::default()
        })
        .id();
    log::warn!("LDTK world bundle id: {:?}", id);

    let id = commands
        .spawn(player::PlayerBundle::new(vec2(10.0, 10.0), &asset_server))
        .insert(los::LosEye {})
        .id();
    log::warn!("Player id: {:?}", id);
}
