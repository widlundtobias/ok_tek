use bevy::prelude::{Mesh, Vec2, Vec3};
use bevy::render::mesh::Indices;

use bevy::render::render_resource::PrimitiveTopology::TriangleList;

pub struct MeshBuilder {
    positions: Vec<Vec3>,
    uvs: Vec<Vec2>,
    normals: Vec<Vec3>,
    indices: Vec<u32>,
}

impl MeshBuilder {
    pub fn new() -> Self {
        Self {
            positions: Vec::new(),
            uvs: Vec::new(),
            normals: Vec::new(),
            indices: Vec::new(),
        }
    }

    pub fn add_triangle(&mut self, positions: [Vec3; 3], uvs: [Vec2; 3]) {
        // Add vertices and indices
        for psn in &positions {
            //   println!("psn {:?}", psn);
            self.positions.push(*psn);
        }
        let start_idx = self.positions.len() as u32 - 3;
        self.indices
            .extend(&[start_idx, start_idx + 1, start_idx + 2]);

        //stubbed in for now ...
        let normal = compute_normal(positions[0], positions[1], positions[2]);
        self.normals.extend([normal, normal, normal]);

        self.uvs.extend(uvs);
    }

    pub fn build(self) -> Mesh {
        let mut mesh = Mesh::new(TriangleList);
        mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, self.positions);
        mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, self.uvs);
        mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, self.normals);
        mesh.set_indices(Some(Indices::U32(self.indices)));
        mesh
    }
}

fn compute_normal(v0: Vec3, v1: Vec3, v2: Vec3) -> Vec3 {
    let edge1 = v1 - v0;
    let edge2 = v2 - v0;

    edge1.cross(edge2)
}

pub fn compute_uv(
    coord: Vec2,
    bounds_start: Vec2,
    bounds_end: Vec2,
    texture_dimensions: Vec2,
) -> Vec2 {
    let uv_worldspace = coord / (bounds_end - bounds_start);
    uv_worldspace / texture_dimensions
}
