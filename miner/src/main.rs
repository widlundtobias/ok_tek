use bevy::prelude::*;
use bevy_flycam::prelude::*;
use heightmap::{update_heightmap, HeightmapResource};

mod heightmap;
mod heightmap_mesh_builder;
mod mesh_builder;

fn main() {
    App::new()
        .add_plugins((DefaultPlugins,))
        .add_plugins(NoCameraPlayerPlugin)
        .insert_resource(ClearColor(Color::rgb(0.1, 0.1, 0.1)))
        .add_systems(Startup, setup)
        .add_systems(Update, update_heightmap)
        .run();
}

const WIDTH: f32 = 1366.0;
const HEIGHT: f32 = 768.0;

fn setup(
    mut windows: Query<&mut Window>,
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>,
) {
    // Set window resolution
    let mut window = windows.single_mut();
    window.resolution.set(WIDTH, HEIGHT);

    // Create our camera
    commands.spawn((
        Camera3dBundle {
            transform: Transform::from_xyz(0.0, 2.0, 0.5).with_rotation(Quat::from_euler(
                EulerRot::XYZ,
                0.0,
                4.5,
                0.0,
            )),
            ..default()
        },
        // Make it possible to control it
        FlyCam,
    ));

    // Spawn our cylinder entity
    let cylinder_mesh = meshes.add(shape::Cylinder::default().into());
    commands.spawn((PbrBundle {
        mesh: cylinder_mesh,
        ..default()
    },));

    // Spawn a point light entity
    commands.spawn(PointLightBundle {
        point_light: PointLight {
            intensity: 9000.0,
            range: 100.,
            shadows_enabled: true,
            ..default()
        },
        transform: Transform::from_xyz(8.0, 16.0, 8.0),
        ..default()
    });

    let texture: Handle<Image> = asset_server.load("satellite.png");
    let material = materials.add(StandardMaterial {
        base_color_texture: Some(texture),
        unlit: true,
        ..default()
    });

    let heightmap: Handle<Image> = asset_server.load("elevation_blurry_gray.png");
    commands.insert_resource(HeightmapResource {
        heightmap,
        material,
    });

    commands.insert_resource(MovementSettings {
        speed: 120.0,
        ..Default::default()
    });
}
