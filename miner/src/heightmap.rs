use bevy::{prelude::*, render::render_resource::TextureFormat};
use ok_container::grid::Grid2d;

use crate::heightmap_mesh_builder::HeightmapMeshBuilder;

#[derive(Resource)]
pub struct HeightmapResource {
    pub heightmap: Handle<Image>,
    pub material: Handle<StandardMaterial>,
}

pub type Heightmap = Grid2d<u8>;

pub fn update_heightmap(
    mut ev_asset: EventReader<AssetEvent<Image>>,
    assets: ResMut<Assets<Image>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut commands: Commands,
    heightmap_image: Res<HeightmapResource>,
) {
    for ev in ev_asset.iter() {
        if let AssetEvent::Created { handle } = ev {
            if *handle == heightmap_image.heightmap {
                let image = assets.get(handle).unwrap();
                let heightmap = load_from_image(image).unwrap();
                let heightmap = HeightmapMeshBuilder::new(&heightmap)
                    .with_lateral_scale(0.25)
                    .with_lod_level(2)
                    .build();
                let heightmap_mesh = meshes.add(heightmap);

                commands.spawn((PbrBundle {
                    mesh: heightmap_mesh,
                    material: heightmap_image.material.clone(),
                    transform: Transform::from_xyz(-100.0, -230.0, -100.0),
                    ..default()
                },));
            }
        }
    }
}

fn load_from_image(image: &Image) -> Result<Heightmap, ()> {
    let width = image.size().x as _;
    let height = image.size().y as _;

    let format = image.texture_descriptor.format;

    if format != TextureFormat::Rgba8UnormSrgb {
        return Err(());
    }

    // With the format being Rgba8UnormSrgb, and gray scale
    // just grab every 4 pixels
    let mut height_map = Heightmap::new_with_size(IVec2::new(width, height), 0);

    for y in 0..height {
        for x in 0..width {
            let index = (4 * (y * width + x)) as usize; // 2 because of Rgba8
            let height_value = image.data[index];
            height_map.set(x, y, height_value).unwrap();
        }
    }

    Ok(height_map)
}
