use bevy::prelude::*;

use crate::{
    heightmap::Heightmap,
    mesh_builder::{compute_uv, MeshBuilder},
};

pub struct HeightmapMeshBuilder<'a> {
    heightmap: &'a Heightmap,
    height_scale: f32,
    lateral_scale: f32,
    lod_level: u32,
}

impl<'a> HeightmapMeshBuilder<'a> {
    pub fn new(heightmap: &'a Heightmap) -> Self {
        Self {
            heightmap,
            height_scale: 1.0,
            lateral_scale: 1.0,
            lod_level: 0,
        }
    }

    pub fn with_height_scale(self, height_scale: f32) -> Self {
        Self {
            height_scale,
            ..self
        }
    }

    pub fn with_lateral_scale(self, lateral_scale: f32) -> Self {
        Self {
            lateral_scale,
            ..self
        }
    }

    pub fn with_lod_level(self, lod_level: u32) -> Self {
        Self { lod_level, ..self }
    }

    pub fn build(self) -> Mesh {
        let mut builder = MeshBuilder::new();

        let step_size: i32 = 1 << self.lod_level; // doubles the step for each LOD level using bit shifting

        let bounds_pct_start = Vec2::splat(0.0);
        let bounds_pct_end = Vec2::splat(1.0);

        let heightmap_width = self.heightmap.width();
        let heightmap_height = self.heightmap.height();

        let texture_dimensions = Vec2::new(heightmap_width as f32, heightmap_height as f32);
        let width = texture_dimensions[0];
        let height = texture_dimensions[1];

        for x in (0..heightmap_width).step_by(step_size as _) {
            for y in (0..heightmap_height).step_by(step_size as _) {
                let fx = x as f32;
                let fz = y as f32;

                //cant sample so we just continue
                if x + step_size >= width as i32 {
                    continue;
                }
                if y + step_size >= height as i32 {
                    continue;
                }

                if x + step_size >= heightmap_width {
                    continue;
                }
                if y + step_size >= heightmap_height {
                    continue;
                }

                let lb = *self.heightmap.get(x, y).unwrap() as f32 * self.height_scale;
                let lf = *self.heightmap.get(x, y + step_size).unwrap() as f32 * self.height_scale;
                let rb = *self.heightmap.get(x + step_size, y).unwrap() as f32 * self.height_scale;
                let rf = *self.heightmap.get(x + step_size, y + step_size).unwrap() as f32
                    * self.height_scale;

                let uv_lb = compute_uv(
                    Vec2::new(fx, fz),
                    bounds_pct_start,
                    bounds_pct_end,
                    texture_dimensions,
                );
                let uv_rb = compute_uv(
                    Vec2::new(fx + step_size as f32, fz),
                    bounds_pct_start,
                    bounds_pct_end,
                    texture_dimensions,
                );
                let uv_rf = compute_uv(
                    Vec2::new(fx + step_size as f32, fz + step_size as f32),
                    bounds_pct_start,
                    bounds_pct_end,
                    texture_dimensions,
                );
                let uv_lf = compute_uv(
                    Vec2::new(fx, fz + step_size as f32),
                    bounds_pct_start,
                    bounds_pct_end,
                    texture_dimensions,
                );

                let left_back = Vec3::new((fx) * self.lateral_scale, lb, fz * self.lateral_scale);
                let right_back = Vec3::new(
                    (fx + step_size as f32) * self.lateral_scale,
                    rb,
                    fz * self.lateral_scale,
                );
                let right_front = Vec3::new(
                    (fx + step_size as f32) * self.lateral_scale,
                    rf,
                    (fz + step_size as f32) * self.lateral_scale,
                );
                let left_front = Vec3::new(
                    (fx) * self.lateral_scale,
                    lf,
                    (fz + step_size as f32) * self.lateral_scale,
                );

                builder.add_triangle([left_front, right_back, left_back], [uv_lf, uv_rb, uv_lb]);
                builder.add_triangle([right_front, right_back, left_front], [uv_rf, uv_rb, uv_lf]);
            }
        }

        builder.build()
    }
}

impl From<HeightmapMeshBuilder<'_>> for Mesh {
    fn from(value: HeightmapMeshBuilder) -> Self {
        value.build()
    }
}
