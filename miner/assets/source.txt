These images are downloaded by rightclicking on this map: https://portal.ga.gov.au/restore/063b2e1b-196c-43d5-87d8-351793e98cd5

satellite.png is the world layer
elevation.png/elevation2.png are from two different elevation layers

the elevation_blurry_gray.png and elevation_fine_gray.png are gray-scalified and contrastified versions of the above, through GIMP