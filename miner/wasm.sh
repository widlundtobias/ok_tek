mkdir -p web
cargo build --release --target=wasm32-unknown-unknown
wasm-bindgen --out-name miner \
  --out-dir web \
  --target web target/wasm32-unknown-unknown/release/miner.wasm
cp index.html web/