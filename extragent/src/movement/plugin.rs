use bevy::prelude::{IntoSystemConfigs, Update};

use crate::system_sets::SystemSets;

use super::{update_look_at, update_movement};

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_systems(
            Update,
            (update_movement, update_look_at).in_set(SystemSets::Movement),
        );
    }
}
