use bevy::{
    math::{vec2, Vec3Swizzles},
    prelude::{Component, Quat, Query, Transform, Vec2},
};

#[derive(Debug, PartialEq, Clone, Copy, Component)]
pub struct LookAt {
    pub position: Option<Vec2>,
}

impl LookAt {
    pub fn new_empty() -> Self {
        Self { position: None }
    }
}

pub fn update_look_at(mut query: Query<(&mut Transform, &LookAt)>) {
    for (mut transform, look_at) in query.iter_mut() {
        let pos = transform.translation.xy();
        if let Some(target) = look_at.position {
            let angle = (target - pos).angle_between(vec2(1.0, 0.0));
            transform.rotation = Quat::from_rotation_z(-angle);
        }
    }
}
