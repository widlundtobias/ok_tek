use bevy::prelude::*;

use bevy_rapier2d::prelude::KinematicCharacterController;
use ok_math::{safe_normalize, ZeroVecError};

use crate::player;

#[derive(Debug, PartialEq, Clone, Copy, Component)]
pub struct MoveIntention {
    direction: Option<Vec2>,
}

impl Eq for MoveIntention {}

impl MoveIntention {
    pub fn new(direction: Option<Vec2>) -> Result<Self, ZeroVecError> {
        let direction = match direction {
            Some(v) => Some(safe_normalize(v)?),
            None => None,
        };

        Ok(MoveIntention { direction })
    }

    pub fn new_empty() -> Self {
        Self { direction: None }
    }

    pub fn direction(&self) -> &Option<Vec2> {
        &self.direction
    }
}

// TODO: place somewhere nice
#[derive(Component)]
pub struct Recoil {
    pub vel: Vec2,
    pub duration: f32,
}

#[allow(clippy::type_complexity)]
pub fn update_movement(
    time: Res<Time>,
    mut commands: Commands,
    mut query: Query<(
        Entity,
        &mut KinematicCharacterController,
        &MoveIntention,
        Option<&mut Recoil>,
    )>,
) {
    for (entity, mut controller, move_intention, recoil) in query.iter_mut() {
        let direction = move_intention.direction().unwrap_or_default();
        let vel = direction * player::PLAYER_SPEED; // TODO: break out to a proper stats struct

        let mut final_velocity = vel;

        if let Some(mut recoil) = recoil {
            final_velocity += recoil.vel;
            recoil.duration -= time.delta_seconds();
            if recoil.duration <= 0.0 {
                commands.entity(entity).remove::<Recoil>();
            }
        }

        // Set the desired move-by amount in the character controller.
        // Bad name on that variable.
        if final_velocity.length() != 0.0 {
            controller.translation = Some(final_velocity * time.delta_seconds());
        }
    }
}
