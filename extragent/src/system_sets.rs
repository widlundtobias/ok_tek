use bevy::prelude::SystemSet;

#[derive(SystemSet, Debug, Hash, PartialEq, Eq, Clone)]
pub enum SystemSets {
    PlayerInput,
    Movement,
    Weapon,
}
