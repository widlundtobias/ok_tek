use bevy::prelude::{Component, Entity};

#[derive(Component)]
pub struct Hands {
    pub left: Option<Entity>,
}
impl Hands {
    pub fn new_with_equipped(item: Entity) -> Self {
        Self { left: Some(item) }
    }
}
