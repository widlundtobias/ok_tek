use bevy::{
    math::{vec2, Vec3Swizzles},
    prelude::*,
};
use rand::Rng;

use crate::{movement::Recoil, rendering::camera::CameraShake};

use super::BulletBundle;

pub mod predefined;

mod bundle;
mod stats;

pub use bundle::*;
pub use stats::*;

#[derive(Debug, PartialEq, Clone, Copy, Component)]
pub struct Gun {
    pub want_trigger: bool,
    pub cool_down: f32,
    pub stats: Stats,
}

impl Gun {
    pub fn new_with_stats(stats: Stats) -> Self {
        Self {
            want_trigger: false,
            cool_down: 0.0,
            stats,
        }
    }
}

pub fn update_gun(
    time: Res<Time>,
    mut camera_shake: ResMut<CameraShake>,
    mut query: Query<(&GlobalTransform, &mut Gun, &Parent)>,
    mut commands: Commands,
) {
    for (transform, mut gun, parent) in query.iter_mut() {
        if gun.cool_down > 0.0 {
            gun.cool_down -= time.delta_seconds();
            gun.cool_down = gun.cool_down.max(0.0);
        }

        if gun.want_trigger {
            if !gun.stats.automatic {
                gun.want_trigger = false;
            }

            let should_fire = gun.cool_down <= 0.0;

            if should_fire {
                let transform = transform.compute_transform();

                let position = transform.translation.xy();
                let gun_direction = (transform.rotation * vec2(1.0, 0.0).extend(1.0)).xy();

                let mut rng = rand::thread_rng();

                for _ in 0..gun.stats.bullet_count {
                    let spread_angle = (std::f32::consts::PI * 2.0) * gun.stats.spread / 100.0;
                    let half_spread_angle = spread_angle / 2.0;
                    let deviation = rng.gen_range(-half_spread_angle..=half_spread_angle);
                    let direction = ok_math::rotate_vec(&gun_direction, deviation);
                    let spawn_offset = 3.0;
                    let position = position + direction * spawn_offset;
                    commands.spawn(BulletBundle::new(position, direction));
                }

                gun.cool_down = 1.0 / gun.stats.fire_rate;

                commands.entity(parent.get()).insert(Recoil {
                    vel: -gun_direction * gun.stats.recoil,
                    duration: 0.1,
                });

                let max_min_intensity = 0.5;
                let max_max_intensity = 5.0;
                let max_duration = 1.0;
                let min_intensity = max_min_intensity * gun.stats.camera_shake_intensity / 100.0;
                let max_intensity = max_max_intensity * gun.stats.camera_shake_intensity / 100.0;
                let duration = max_duration * gun.stats.camera_shake_intensity / 100.0;
                camera_shake.shake(min_intensity..max_intensity, duration);
            }
        }
    }
}
