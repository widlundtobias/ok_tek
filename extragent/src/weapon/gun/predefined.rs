use super::{Gun, Stats};

pub fn pistol() -> Gun {
    let stats = Stats::new(7.0, false, 6.0, 1, 2.0, 3.0);
    Gun::new_with_stats(stats)
}

pub fn shotgun() -> Gun {
    let stats = Stats::new(10.0, false, 1.0, 5, 20.0, 10.0);
    Gun::new_with_stats(stats)
}

pub fn rifle() -> Gun {
    let stats = Stats::new(1.0, false, 0.8, 1, 3.0, 3.0);
    Gun::new_with_stats(stats)
}

pub fn smg() -> Gun {
    let stats = Stats::new(7.0, true, 10.0, 1, 1.0, 2.0);
    Gun::new_with_stats(stats)
}

pub fn machine_gun() -> Gun {
    let stats = Stats::new(5.0, true, 15.0, 2, 10.0, 10.0);
    Gun::new_with_stats(stats)
}
