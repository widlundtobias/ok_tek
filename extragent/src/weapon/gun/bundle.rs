use bevy::prelude::*;

use super::Gun;

#[derive(Bundle)]
pub struct GunBundle {
    pub gun: Gun,
    pub spatial: SpatialBundle,
}

impl GunBundle {
    pub fn new(gun: Gun) -> Self {
        Self {
            gun,
            spatial: Default::default(),
        }
    }
}
