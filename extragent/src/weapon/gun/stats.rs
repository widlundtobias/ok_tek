#[derive(Debug, PartialEq, Clone, Copy)]

pub struct Stats {
    pub spread: f32,                 // Stat 0..100 spread percent
    pub automatic: bool,             // True if this weapon is automatic
    pub fire_rate: f32,              // Times per second
    pub bullet_count: i32,           // How many bullets to fire
    pub recoil: f32,                 // How strongly the player jolts backwards
    pub camera_shake_intensity: f32, // How much the camera shakes. 0-100 expected range, can go over.
}

impl Stats {
    pub fn new(
        spread: f32,
        automatic: bool,
        fire_rate: f32,
        bullet_count: i32,
        recoil: f32,
        camera_shake_intensity: f32,
    ) -> Self {
        Self {
            spread,
            automatic,
            fire_rate,
            bullet_count,
            recoil,
            camera_shake_intensity,
        }
    }
}
