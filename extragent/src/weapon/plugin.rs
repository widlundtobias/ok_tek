use bevy::prelude::{IntoSystemConfigs, Update};

use crate::system_sets::SystemSets;

use super::{bullet_collisions, gun};

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_systems(
            Update,
            // TODO: break out bullet collisions into a generic collision system funnel
            (gun::update_gun, bullet_collisions).in_set(SystemSets::Weapon),
        );
    }
}
