use bevy::{prelude::*, utils::HashSet};
use bevy_particle_systems::{
    CircleSegment, ColorOverTime, JitteredValue, ParticleSystem, ParticleSystemBundle,
    ParticleTexture, Playing,
};
use bevy_rapier2d::prelude::*;
use rand::Rng;
use smallvec::SmallVec;

use crate::{
    player::Player,
    world::{structural, DOOR_PARTICLE_COLOR},
};

use super::{Bullet, BULLET_COLOR};

// TODO: I guess this needs to be a general collision detect thing that runs after movements, and before resolving
// stuff, and it can do so by setting components or sending further more detailed events
pub fn bullet_collisions(
    mut collision_events: EventReader<CollisionEvent>,
    bullet_query: Query<(&Transform, With<Bullet>)>,
    player_query: Query<With<Player>>,
    mut integrity_query: Query<(
        &mut structural::Integrity,
        Option<&mut structural::PanelHoles>,
    )>,
    asset_server: Res<AssetServer>,
    mut commands: Commands,
) {
    let mut despawned = HashSet::new();

    for collision_event in collision_events.iter() {
        if let CollisionEvent::Started(a, b, _) = collision_event {
            if despawned.contains(a) || despawned.contains(b) {
                continue;
            }

            let result = resolve_bullet_collision(
                a,
                b,
                &bullet_query,
                &player_query,
                &mut integrity_query,
                &asset_server,
                &mut commands,
            );

            despawned.extend(result.despawned_entities());

            if !result.did_despawn(a) && !result.did_despawn(b) {
                let result = resolve_bullet_collision(
                    b,
                    a,
                    &bullet_query,
                    &player_query,
                    &mut integrity_query,
                    &asset_server,
                    &mut commands,
                );

                despawned.extend(result.despawned_entities());
            }
        }
    }
}

#[derive(Clone)]
enum BulletCollisionCheckResult {
    Nothing,
    Despawned(SmallVec<[Entity; 2]>),
}

impl BulletCollisionCheckResult {
    fn did_despawn(&self, entity: &Entity) -> bool {
        match self {
            BulletCollisionCheckResult::Nothing => false,
            BulletCollisionCheckResult::Despawned(despawned) => despawned.contains(entity),
        }
    }

    fn despawned_entities(&self) -> SmallVec<[Entity; 2]> {
        match self {
            BulletCollisionCheckResult::Nothing => Default::default(),
            BulletCollisionCheckResult::Despawned(despawned) => despawned.clone(),
        }
    }
}

fn resolve_bullet_collision(
    bullet: &Entity,
    target: &Entity,
    bullet_query: &Query<(&Transform, With<Bullet>)>,
    player_query: &Query<With<Player>>,
    integrity_query: &mut Query<(
        &mut structural::Integrity,
        Option<&mut structural::PanelHoles>,
    )>,
    asset_server: &Res<AssetServer>,
    commands: &mut Commands,
) -> BulletCollisionCheckResult {
    if let Ok((bullet_transform, _)) = bullet_query.get(*bullet) {
        let is_target_player = player_query.contains(*target);
        let is_target_bullet = bullet_query.contains(*target);

        if !is_target_player && !is_target_bullet {
            commands.entity(*bullet).despawn();

            let mut result = SmallVec::new();
            result.push(*bullet);

            // Damage structural integrity
            if let Ok((mut integrity, panel_holes)) = integrity_query.get_mut(*target) {
                integrity.0 -= 10;

                if integrity.0 <= 0 {
                    commands.entity(*target).despawn_recursive();

                    result.push(*target);

                    commands
                        .spawn(ParticleSystemBundle {
                            particle_system: ParticleSystem {
                                texture: ParticleTexture::Sprite(asset_server.load("blank.png")),
                                color: ColorOverTime::Constant(DOOR_PARTICLE_COLOR),
                                spawn_rate_per_second: 300.0.into(),
                                initial_speed: JitteredValue::jittered(150.0, -100.0..100.0),
                                lifetime: JitteredValue::jittered(0.5, -0.1..0.1),
                                emitter_shape: CircleSegment {
                                    radius: 2.0.into(),
                                    opening_angle: std::f32::consts::PI / 2.0,
                                    direction_angle: 0.0, // This can be zero since it uses the rotation of the transform of the whole particle system
                                }
                                .into(),
                                looping: false,
                                scale: 0.5.into(),
                                system_duration_seconds: 0.05,
                                initial_rotation: (-90.0_f32).to_radians().into(),
                                rotate_to_movement_direction: true,
                                z_value_override: Some((10.0).into()),
                                despawn_on_finish: true,
                                ..ParticleSystem::default()
                            },
                            transform: *bullet_transform,
                            ..ParticleSystemBundle::default()
                        })
                        .insert(Playing);
                } else if integrity.damaged_level().is_rekt() {
                    commands.entity(*target).remove::<structural::Hinge>();
                }

                if let Some(mut panel_holes) = panel_holes {
                    let mut rng = rand::thread_rng();

                    let random_chance = rng.gen_bool(0.33);

                    if random_chance {
                        panel_holes.create_random_hole(&mut rng);
                    }
                }

                commands
                    .spawn(ParticleSystemBundle {
                        particle_system: ParticleSystem {
                            texture: ParticleTexture::Sprite(asset_server.load("blank.png")),
                            color: ColorOverTime::Constant(DOOR_PARTICLE_COLOR),
                            spawn_rate_per_second: 50.0.into(),
                            initial_speed: JitteredValue::jittered(150.0, -100.0..100.0),
                            lifetime: JitteredValue::jittered(0.05, -0.01..0.01),
                            emitter_shape: CircleSegment {
                                radius: 2.0.into(),
                                opening_angle: std::f32::consts::PI / 2.0 + std::f32::consts::PI,
                                direction_angle: 0.0, // This can be zero since it uses the rotation of the transform of the whole particle system
                            }
                            .into(),
                            looping: false,
                            scale: 0.5.into(),
                            system_duration_seconds: 0.05,
                            initial_rotation: (-90.0_f32).to_radians().into(),
                            rotate_to_movement_direction: true,
                            z_value_override: Some((10.0).into()),
                            despawn_on_finish: true,
                            ..ParticleSystem::default()
                        },
                        transform: *bullet_transform,
                        ..ParticleSystemBundle::default()
                    })
                    .insert(Playing);
            }

            commands
                .spawn(ParticleSystemBundle {
                    particle_system: ParticleSystem {
                        texture: ParticleTexture::Sprite(asset_server.load("blank.png")),
                        color: ColorOverTime::Constant(BULLET_COLOR),
                        spawn_rate_per_second: 50.0.into(),
                        initial_speed: JitteredValue::jittered(150.0, -100.0..100.0),
                        lifetime: JitteredValue::jittered(0.05, -0.01..0.01),
                        emitter_shape: CircleSegment {
                            radius: 2.0.into(),
                            opening_angle: std::f32::consts::PI / 2.0,
                            direction_angle: 0.0, // This can be zero since it uses the rotation of the transform of the whole particle system
                        }
                        .into(),
                        looping: false,
                        scale: 0.5.into(),
                        system_duration_seconds: 0.05,
                        initial_rotation: (-90.0_f32).to_radians().into(),
                        rotate_to_movement_direction: true,
                        z_value_override: Some((10.0).into()),
                        despawn_on_finish: true,
                        ..ParticleSystem::default()
                    },
                    transform: *bullet_transform,
                    ..ParticleSystemBundle::default()
                })
                .insert(Playing);

            return BulletCollisionCheckResult::Despawned(result);
        }
    }
    BulletCollisionCheckResult::Nothing
}
