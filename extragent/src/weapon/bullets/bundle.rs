use bevy::{math::vec2, prelude::*};
use bevy_rapier2d::prelude::*;

use super::Bullet;

#[derive(Bundle)]
pub struct BulletBundle {
    pub collider: Collider,
    pub sensor: Sensor,
    pub active_events: ActiveEvents,
    pub rigid_body: RigidBody,
    pub velocity: Velocity,
    pub rotation_constraints: LockedAxes,
    pub sprite_bundle: SpriteBundle,
    pub bullet: Bullet,
}

pub const BULLET_COLOR: Color = Color::rgb(0.9, 0.8, 0.1);

impl BulletBundle {
    pub fn new(position: Vec2, direction: Vec2) -> Self {
        const SPEED: f32 = 500.0;

        let size = vec2(10.0, 1.0);

        let angle = direction.angle_between(vec2(1.0, 0.0));
        let rot = Quat::from_rotation_z(-angle);

        Self {
            collider: Collider::cuboid(size.x / 2.0, size.y / 2.0),
            sensor: Sensor,
            active_events: ActiveEvents::COLLISION_EVENTS,
            rigid_body: RigidBody::Dynamic,
            rotation_constraints: LockedAxes::ROTATION_LOCKED,
            sprite_bundle: SpriteBundle {
                sprite: Sprite {
                    color: BULLET_COLOR,
                    custom_size: Some(size),
                    ..default()
                },
                transform: Transform::default()
                    .with_translation(position.extend(100.0))
                    .with_rotation(rot),
                ..Default::default()
            },
            velocity: Velocity {
                linvel: direction * SPEED,
                ..Default::default()
            },
            bullet: Bullet,
        }
    }
}
