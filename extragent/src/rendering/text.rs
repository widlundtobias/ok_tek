use bevy::prelude::*;

#[derive(Component)]
pub struct TextNoticeTimeToLive {
    pub time: f32,
}

impl TextNoticeTimeToLive {
    pub fn new(time: f32) -> Self {
        Self { time }
    }
}

pub fn update_text_time_to_live(
    mut text_query: Query<(Entity, &mut TextNoticeTimeToLive)>,
    mut commands: Commands,
    time: Res<Time>,
) {
    for (entity, mut ttl) in text_query.iter_mut() {
        if ttl.time > 0.0 {
            ttl.time -= time.delta_seconds();
        }
        if ttl.time <= 0.0 {
            commands.entity(entity).despawn()
        }
    }
}
