use bevy::{math::vec2, prelude::*};
use rand::Rng;

#[derive(Component)]
pub struct WorldCamera;

#[derive(Component)]
pub struct CameraFollow {
    followee: Entity,
}

impl CameraFollow {
    pub fn new(id: Entity) -> Self {
        Self { followee: id }
    }
}

pub fn camera_follow(
    mut cam_query: Query<(&mut Transform, &CameraFollow)>,
    followee_query: Query<(&Transform, Without<CameraFollow>)>,
) {
    for (mut camera_transform, camera_follow) in cam_query.iter_mut() {
        if let Ok((target_transform, _)) = followee_query.get(camera_follow.followee) {
            camera_transform.translation = target_transform.translation;
        } else {
            warn!(
                "Missing target in camera follow. Camera trying to follow entity {:?}",
                camera_follow.followee
            );
        }
    }
}

#[derive(Resource, Default)]
pub struct CameraShake {
    pub intensity: std::ops::Range<f32>,
    /// In seconds
    pub duration: f32,
}

impl CameraShake {
    pub fn shake(&mut self, intensity: std::ops::Range<f32>, duration: f32) {
        self.intensity = intensity;
        self.duration = duration;
    }
}

#[derive(Component)]
pub struct ShakeableCamera;

pub fn camera_shake(
    mut cam_query: Query<(&mut Transform, &ShakeableCamera)>,
    time: Res<Time>,
    mut cam_shake: ResMut<CameraShake>,
) {
    if !cam_shake.intensity.is_empty() {
        let mut rng = rand::thread_rng();

        let intensity = rng.gen_range(cam_shake.intensity.clone());
        let angle = rng.gen_range(0.0..=std::f32::consts::PI * 2.0);

        for (mut camera_transform, _) in cam_query.iter_mut() {
            if cam_shake.duration > 0.0 {
                let offset = Quat::from_rotation_z(angle) * vec2(1.0 * intensity, 0.0).extend(0.0);
                camera_transform.translation += offset;
            }
        }
    }

    if cam_shake.duration > 0.0 {
        cam_shake.duration -= time.delta_seconds();
        cam_shake.duration = cam_shake.duration.max(0.0);
    }
}

#[derive(Component)]
pub struct CameraTruncate;

pub fn camera_truncate(mut cam_query: Query<(&mut Transform, With<CameraTruncate>)>) {
    for (mut camera_transform, _) in cam_query.iter_mut() {
        camera_transform.translation.x = camera_transform.translation.x.trunc();
        camera_transform.translation.y = camera_transform.translation.y.trunc();
    }
}
