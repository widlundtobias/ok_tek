use bevy::prelude::{IntoSystemConfigs, Update};

use crate::system_sets::SystemSets;

use super::{process_player_input_keys, process_player_look_at};

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_systems(
            Update,
            (process_player_input_keys, process_player_look_at).in_set(SystemSets::PlayerInput),
        );
    }
}
