use bevy::prelude::*;

/// An entity with this component has the semantics of an entity controlled by a player
#[derive(Component)]
pub struct Player {}
