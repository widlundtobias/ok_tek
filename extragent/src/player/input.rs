use bevy::{
    math::{vec2, Vec3Swizzles},
    prelude::*,
    text::DEFAULT_FONT_HANDLE,
};

use crate::{
    equipper, movement, rendering,
    weapon::{self, gun::predefined},
    world::structural,
};

use super::Player;

pub fn process_player_input_keys(
    keyboard_input: Res<Input<KeyCode>>,
    mouse_buttons: Res<Input<MouseButton>>,
    mut q_move_intention: Query<(&mut movement::MoveIntention, &Transform, With<Player>)>,
    mut q_hands: Query<(Entity, &mut equipper::Hands, With<Player>)>,
    mut q_gun: Query<&mut weapon::gun::Gun>,
    mut q_hinge: Query<(&mut structural::Hinge, &Transform)>,
    mut commands: Commands,
) {
    // Movement
    let as_direction = |pressed, direction| if pressed { direction } else { Vec2::ZERO };

    let left = keyboard_input.pressed(KeyCode::A);
    let right = keyboard_input.pressed(KeyCode::D);
    let up = keyboard_input.pressed(KeyCode::W);
    let down = keyboard_input.pressed(KeyCode::S);

    let direction = as_direction(left, vec2(-1.0, 0.0))
        + as_direction(right, vec2(1.0, 0.0))
        + as_direction(up, vec2(0.0, 1.0))
        + as_direction(down, vec2(0.0, -1.0));

    let direction = if direction.length() == 0.0 {
        None
    } else {
        Some(direction)
    };

    let mut player_pos = None;

    for (mut move_intention, transform, _) in q_move_intention.iter_mut() {
        player_pos = Some(transform.translation.xy());
        *move_intention = movement::MoveIntention::new(direction).unwrap();
    }

    // Door opening
    const DOOR_REACH_DISTANCE: f32 = 10.0;
    let want_open_door = keyboard_input.just_pressed(KeyCode::G);
    if want_open_door {
        if let Some((mut hinge, _)) = q_hinge
            .iter_mut()
            .filter_map(|(hinge, transform)| {
                let door_pos = transform.translation.xy();
                let player_pos = player_pos.unwrap_or_default();
                let distance = player_pos.distance(door_pos);
                if distance < DOOR_REACH_DISTANCE {
                    Some((hinge, distance))
                } else {
                    None
                }
            })
            .min_by_key(|(_, distance)| ordered_float::OrderedFloat(*distance))
        {
            hinge.toggle();
        }
    }

    // Attacking

    if let Ok((player, mut hands, _)) = q_hands.get_single_mut() {
        if let Some(equipped_gun) = hands.left {
            if let Ok(mut equipped_gun) = q_gun.get_mut(equipped_gun) {
                // Firing. TODO: replace with sending a message maybe
                if mouse_buttons.just_pressed(MouseButton::Left) {
                    equipped_gun.want_trigger = true;
                }

                // Not firing. TODO: replace with sending a message maybe
                if mouse_buttons.just_released(MouseButton::Left) {
                    equipped_gun.want_trigger = false;
                }
            }

            // Weapon switching
            let switch_to = if keyboard_input.pressed(KeyCode::Key1) {
                Some((predefined::pistol(), "pistol"))
            } else if keyboard_input.pressed(KeyCode::Key2) {
                Some((predefined::shotgun(), "shotgun"))
            } else if keyboard_input.pressed(KeyCode::Key3) {
                Some((predefined::rifle(), "rifle"))
            } else if keyboard_input.pressed(KeyCode::Key4) {
                Some((predefined::smg(), "smg"))
            } else if keyboard_input.pressed(KeyCode::Key5) {
                Some((predefined::machine_gun(), "machine_gun"))
            } else {
                None
            };

            if let Some((new_gun, switch_text)) = switch_to {
                commands.entity(equipped_gun).despawn();

                let new_gun = commands.spawn(weapon::gun::GunBundle::new(new_gun)).id();

                hands.left = Some(new_gun);
                commands.entity(player).add_child(new_gun);

                commands
                    .spawn((
                        // Create a TextBundle that has a Text with a single section.
                        TextBundle::from_section(
                            // Accepts a `String` or any type that converts into a `String`, such as `&str`
                            switch_text,
                            TextStyle {
                                // This font is loaded and will be used instead of the default font.
                                font: DEFAULT_FONT_HANDLE.typed(),
                                font_size: 20.0,
                                color: Color::RED,
                            },
                        ) // Set the alignment of the Text
                        .with_text_alignment(TextAlignment::Center)
                        // Set the style of the TextBundle itself.
                        .with_style(Style {
                            position_type: PositionType::Absolute,
                            bottom: Val::Px(350.0),
                            right: Val::Px(600.0),
                            ..default()
                        }),
                    ))
                    .insert(rendering::text::TextNoticeTimeToLive::new(1.0));
            }
        }
    }
}

pub fn process_player_look_at(
    mut cursor_evr: EventReader<CursorMoved>,
    mut q_player: Query<(&mut movement::LookAt, With<Player>)>,
    q_camera: Query<(
        &Camera,
        &GlobalTransform,
        With<rendering::camera::WorldCamera>,
    )>,
) {
    for ev in cursor_evr.iter() {
        for (mut look_at, _) in q_player.iter_mut() {
            if let Ok((camera, camera_transform, _)) = q_camera.get_single() {
                if let Some(world_pos) = camera
                    .viewport_to_world_2d(camera_transform, vec2(ev.position.x, ev.position.y))
                {
                    *look_at = movement::LookAt {
                        position: Some(world_pos),
                    };
                }
            }
        }
    }
}
