use bevy::{math::vec2, prelude::*};
use bevy_rapier2d::prelude::*;

use crate::{equipper, movement};

use super::Player;

#[derive(Bundle)]
pub struct PlayerBundle {
    pub move_intention: movement::MoveIntention,
    pub look_at: movement::LookAt,
    pub collider: Collider,
    pub character_controller: KinematicCharacterController,
    pub collision_types: ActiveCollisionTypes,
    pub velocity: Velocity,
    pub sprite_bundle: SpriteBundle,
    pub player: Player,
    pub hands: equipper::Hands,
}

impl PlayerBundle {
    pub fn new(position: Vec2, asset_server: &AssetServer, gun: Entity) -> Self {
        let half_size = vec2(3.0, 3.0);

        Self {
            move_intention: movement::MoveIntention::new_empty(),
            look_at: movement::LookAt::new_empty(),
            collider: Collider::ball(half_size.x),
            character_controller: KinematicCharacterController {
                snap_to_ground: None,
                autostep: None,
                translation: Some(position),
                ..KinematicCharacterController::default()
            },
            collision_types: ActiveCollisionTypes::default()
                | ActiveCollisionTypes::KINEMATIC_STATIC,
            sprite_bundle: SpriteBundle {
                texture: asset_server.load("player.png"),
                transform: Transform::default().with_translation(Vec2::ZERO.extend(100.0)),
                ..Default::default()
            },
            player: Player {},
            velocity: Default::default(),
            hands: equipper::Hands::new_with_equipped(gun),
        }
    }
}
