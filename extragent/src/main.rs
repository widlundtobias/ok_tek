#![allow(clippy::type_complexity)]

use bevy::{
    log,
    math::{vec2, vec3},
    prelude::*,
};
use bevy_ecs_ldtk::prelude::*;
use bevy_particle_systems::ParticleSystemPlugin;
use rendering::camera::CameraShake;
use system_sets::SystemSets;

mod equipper;
mod movement;
mod physics;
mod player;
mod rendering;
mod system_sets;
mod weapon;
mod world;

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins.set(ImagePlugin::default_nearest()),
            movement::Plugin,
            player::Plugin,
            world::Plugin,
            physics::Plugin,
            weapon::Plugin,
            los::Plugin::new(los::LosConfig {
                shadow_opacity: 0.8,
                composite_camera_scale: 0.25,
            }),
            ParticleSystemPlugin,
            //--- If you wanna see physics debug
            // RapierDebugRenderPlugin::default(),
            //--- If you wanna see FPS
            // LogDiagnosticsPlugin::default(),
            // FrameTimeDiagnosticsPlugin::default(),
        ))
        .insert_resource(ClearColor(Color::rgb(0.1, 0.1, 0.1)))
        .add_systems(Startup, setup.after(los::LosSet::SetupRendering))
        .add_systems(
            Update,
            (
                (
                    rendering::camera::camera_follow,
                    rendering::camera::camera_shake,
                    rendering::camera::camera_truncate,
                )
                    .chain(),
                rendering::text::update_text_time_to_live,
            ),
        )
        .init_resource::<CameraShake>()
        .configure_set(Update, SystemSets::PlayerInput.before(SystemSets::Movement))
        .configure_set(Update, SystemSets::PlayerInput.before(SystemSets::Weapon))
        .run();
}

const WIDTH: f32 = 1366.0;
const HEIGHT: f32 = 768.0;

#[derive(Component, Default)]
pub struct BlendPassMesh;

fn setup(
    mut windows: Query<&mut Window>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    los_cameras: Res<los::Cameras>,
) {
    let mut window = windows.single_mut();
    window.resolution.set(WIDTH, HEIGHT);

    let id = commands
        .spawn(LdtkWorldBundle {
            ldtk_handle: asset_server.load("my_project.ldtk"),
            transform: Transform::default().with_translation(vec3(0.0, 0.0, 0.0)),
            visibility: Visibility::Visible,
            ..Default::default()
        })
        .id();
    log::info!("LDTK world bundle id: {:?}", id);

    // Spawn the player's gun
    let gun = commands
        .spawn(weapon::gun::GunBundle::new(
            weapon::gun::predefined::pistol(),
        ))
        .id();

    // Spawn the player
    let player = commands
        .spawn(player::PlayerBundle::new(
            vec2(20.0, 20.0),
            &asset_server,
            gun,
        ))
        .insert(los::LosEye {})
        .id();
    log::info!("Player: {:?}", player);

    // Child the gun to the player
    commands.entity(player).push_children(&[gun]);

    commands
        .entity(los_cameras.world)
        .insert(rendering::camera::CameraFollow::new(player))
        .insert(rendering::camera::ShakeableCamera)
        .insert(rendering::camera::WorldCamera)
        .insert(rendering::camera::CameraTruncate);
    commands
        .entity(los_cameras.shadows)
        .insert(rendering::camera::CameraFollow::new(player))
        .insert(rendering::camera::ShakeableCamera)
        .insert(rendering::camera::CameraTruncate);
}
