use bevy::{math::vec2, prelude::*};

use super::structural;

pub fn normal() -> Rect {
    Rect::from_corners(vec2(0.0, 0.0), vec2(8.0, 8.0))
}

pub fn open() -> Rect {
    Rect::from_corners(vec2(8.0, 0.0), vec2(16.0, 8.0))
}

pub fn damaged() -> Rect {
    Rect::from_corners(vec2(0.0, 8.0), vec2(8.0, 16.0))
}

pub fn rekt() -> Rect {
    Rect::from_corners(vec2(8.0, 8.0), vec2(16.0, 16.0))
}

pub fn graphics(hinge: Option<&structural::Hinge>, integrity: &structural::Integrity) -> Rect {
    let hinge = hinge.map(|h| h.0).unwrap_or(structural::HingeState::Closed);
    match (hinge, integrity.damaged_level()) {
        (structural::HingeState::Open, _) => open(),
        (structural::HingeState::Closed, structural::DamagedLevel::Insignificant) => normal(),
        (structural::HingeState::Closed, structural::DamagedLevel::Damaged) => damaged(),
        (structural::HingeState::Closed, structural::DamagedLevel::Rekt) => rekt(),
    }
}

pub fn update_graphics(
    mut query: Query<(
        &mut Sprite,
        Option<&structural::Hinge>,
        &structural::Integrity,
        Or<(Changed<structural::Hinge>, Changed<structural::Integrity>)>,
    )>,
) {
    for (mut sprite, hinge, integrity, _) in &mut query {
        sprite.rect = Some(graphics(hinge, integrity));
    }
}
