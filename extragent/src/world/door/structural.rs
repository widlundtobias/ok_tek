use bevy::prelude::*;
use bevy_rapier2d::prelude::{Collider, ColliderDisabled, RigidBodyDisabled};

use crate::world::los::LosOpaqueTileExternal;

/// 100 is whole, 0 is broken
#[derive(Component, Clone, Copy)]
pub struct Integrity(pub i32);

impl Integrity {
    pub fn damaged_level(&self) -> DamagedLevel {
        match self.0 {
            ..=35 => DamagedLevel::Rekt,
            36..=75 => DamagedLevel::Damaged,
            76.. => DamagedLevel::Insignificant,
        }
    }
}

impl Default for Integrity {
    fn default() -> Self {
        Self(100)
    }
}

#[derive(Debug)]
pub enum DamagedLevel {
    Insignificant,
    Damaged,
    Rekt,
}

impl DamagedLevel {
    pub fn is_rekt(&self) -> bool {
        match self {
            DamagedLevel::Insignificant | DamagedLevel::Damaged => false,
            DamagedLevel::Rekt => true,
        }
    }
}

#[derive(Default, Clone, Copy, Component)]

pub struct Hinge(pub HingeState);

impl Hinge {
    pub fn toggle(&mut self) -> HingeState {
        self.0 = match self.0 {
            HingeState::Open => HingeState::Closed,
            HingeState::Closed => HingeState::Open,
        };

        self.0
    }

    pub fn is_open(&self) -> bool {
        match self.0 {
            HingeState::Open => true,
            HingeState::Closed => false,
        }
    }
}

#[derive(Default, Clone, Copy)]
pub enum HingeState {
    Open,
    #[default]
    Closed,
}

pub fn open_close(
    mut query: Query<(
        Entity,
        Option<&Collider>,
        Option<&mut LosOpaqueTileExternal>,
        &Hinge,
        Changed<Hinge>,
    )>,
    mut commands: Commands,
) {
    for (entity, collider, mut los_rect, hinge, _) in &mut query {
        if hinge.is_open() {
            if let Some(los_rect) = los_rect.as_mut() {
                los_rect.disable();
            }
            if collider.is_some() {
                commands.entity(entity).insert(ColliderDisabled);
                commands.entity(entity).insert(RigidBodyDisabled);
            }
        } else {
            if let Some(mut los_rect) = los_rect {
                los_rect.enable();
            }
            if collider.is_some() {
                commands.entity(entity).remove::<ColliderDisabled>();
                commands.entity(entity).remove::<RigidBodyDisabled>();
            }
        }
    }
}

/// The bools are like a line along the panel, where true means there
/// is a hole
#[derive(Default, Clone, Copy, Component)]

pub struct PanelHoles(pub [bool; 10]);

impl PanelHoles {
    pub fn create_random_hole(&mut self, rng: &mut impl rand::Rng) {
        let i = rng.gen_range(0..self.0.len());
        self.0[i] = true;
    }

    pub fn first_half(&self) -> &[bool] {
        self.0.split_at(5).0
    }

    pub fn second_half(&self) -> &[bool] {
        self.0.split_at(5).1
    }
}
