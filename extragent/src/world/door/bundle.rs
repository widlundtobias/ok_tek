use bevy::prelude::*;
use bevy_ecs_ldtk::prelude::LdtkEntity;

use crate::{
    physics::{self, WallColliderBundle},
    world::los::LosOpaqueTileExternal,
};

use super::{
    graphics,
    structural::{self, Hinge},
};

pub const DOOR_PARTICLE_COLOR: Color = Color::rgb(147.0 / 255.0, 92.0 / 255.0, 28.0 / 255.0);

#[derive(Default, Bundle)]
pub struct DoorBundle {
    pub collider: physics::WallColliderBundle,
    pub los: LosOpaqueTileExternal,
    pub sprite: SpriteBundle,
    pub integrity: structural::Integrity,
    pub hinge: structural::Hinge,
    pub panel_holes: structural::PanelHoles,
}

impl LdtkEntity for DoorBundle {
    fn bundle_entity(
        entity_instance: &bevy_ecs_ldtk::EntityInstance,
        layer_instance: &bevy_ecs_ldtk::prelude::LayerInstance,
        _tileset: Option<&Handle<Image>>,
        _tileset_definition: Option<&bevy_ecs_ldtk::prelude::TilesetDefinition>,
        asset_server: &AssetServer,
        _texture_atlases: &mut Assets<TextureAtlas>,
    ) -> Self {
        // The Wall
        let collider = WallColliderBundle::new();

        // LOS
        let los = LosOpaqueTileExternal::new_from_ldtk(entity_instance, layer_instance);

        // The Sprite
        let texture = asset_server.load("doors.png");
        let rect = Some(graphics::normal());
        let sprite = SpriteBundle {
            sprite: Sprite {
                rect,
                ..Default::default()
            },
            texture,
            ..Default::default()
        };

        Self {
            collider,
            los,
            sprite,
            integrity: Default::default(),
            hinge: Hinge::default(),
            panel_holes: Default::default(),
        }
    }
}
