use bevy::prelude::*;
use bevy_ecs_ldtk::LdtkIntCell;

#[derive(Default, Bundle, LdtkIntCell)]
pub struct GroundBundle {}
