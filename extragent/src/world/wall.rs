use bevy::prelude::*;
use bevy_ecs_ldtk::LdtkIntCell;

use crate::physics;

use super::los::LosOpaqueTile;

#[derive(Default, Bundle, LdtkIntCell)]
pub struct WallBundle {
    #[from_int_grid_cell]
    pub collider_bundle: physics::WallColliderBundle,
    pub los_opaque: LosOpaqueTile,
}
