use bevy::{
    math::{ivec2, vec2},
    prelude::*,
};
use bevy_ecs_ldtk::EntityInstance;
use bevy_ecs_tilemap::tiles::TilePos;
use ok_container::grid::Grid2d;
use ok_math::{LineSegment, Quad};

use super::{structural, TILE_WIDTH};

struct SolidTile<'a> {
    coord: IVec2,
    holes: Option<&'a structural::PanelHoles>,
}

#[derive(Clone, Copy)]
struct SolidTileCell<'a> {
    is_solid: bool,
    holes: Option<&'a structural::PanelHoles>,
}

pub(crate) fn provide_los_obstacles(
    q_tiles: Query<(&TilePos, &LosOpaqueTile)>,
    q_tile_fillers: Query<(&LosOpaqueTileExternal, Option<&structural::PanelHoles>)>,
    mut obstacles: ResMut<los::LosObstacles>,
) {
    let mut all_lines = Vec::new();

    let mut solid_tiles: Vec<_> = q_tiles
        .iter()
        .map(|(tile_pos, _)| SolidTile {
            coord: ivec2(tile_pos.x as i32, tile_pos.y as i32),
            holes: None,
        })
        .collect();

    solid_tiles.extend(q_tile_fillers.iter().filter_map(|(tile, holes)| {
        tile.enabled.then_some(SolidTile {
            coord: ivec2(tile.tile_pos.x, tile.tile_pos.y),
            holes,
        })
    }));

    if let Some(largest_tile) = solid_tiles
        .iter()
        .max_by_key(|tile| (tile.coord.x, tile.coord.y))
    {
        let mut solid_grid = Grid2d::new_with_size(
            IVec2::new(largest_tile.coord.x + 1, largest_tile.coord.y + 1),
            SolidTileCell {
                is_solid: false,
                holes: largest_tile.holes,
            },
        );

        for solid_tile in &solid_tiles {
            // Unwrap OK here since we have just created the grid to encompass
            // all positions so if this crashes, there is a bug very locally
            solid_grid
                .set(
                    solid_tile.coord.x,
                    solid_tile.coord.y,
                    SolidTileCell {
                        is_solid: true,
                        holes: solid_tile.holes,
                    },
                )
                .unwrap();
        }

        let mut lines = Vec::new();

        for cell in solid_grid.iter_with_neighbors() {
            let is_solid = cell.value.is_solid;
            if is_solid {
                // Some(&false) is the only value that's empty
                // since Some(&true) means a solid neighbor
                // and None means out of range. So this means
                // we need to generate cover if it's NOT empty
                let is_left_solid = cell.left.map(|c| c.is_solid) != Some(false);
                let is_right_solid = cell.right.map(|c| c.is_solid) != Some(false);
                let is_top_solid = cell.top.map(|c| c.is_solid) != Some(false);
                let is_bottom_solid = cell.bottom.map(|c| c.is_solid) != Some(false);

                // This cell is solid, so it needs to produce some line of sight lines
                let no_solid_neighbors =
                    !is_left_solid && !is_right_solid && !is_top_solid && !is_bottom_solid;

                let half_tile_width = TILE_WIDTH / 2.0;
                let tile_center =
                    cell.coordinate.as_vec2() * TILE_WIDTH + Vec2::splat(half_tile_width);

                if no_solid_neighbors {
                    // No solid neighbors at all, so make it produce a smaller box of itself
                    let cover_factor = 0.4;
                    let half_factor = cover_factor / 2.0;
                    let start = tile_center - Vec2::splat(TILE_WIDTH * half_factor);
                    let size = Vec2::splat(TILE_WIDTH * half_factor * 2.0);

                    lines.extend(Quad::with_start_size(start, size).line_segments());
                } else {
                    // Some neighbors are solid, so we need to make sight blocking lines pointing out from the center
                    // towards the solid blocks

                    if is_left_solid {
                        let start = tile_center + vec2(-half_tile_width, 0.0);
                        let end = tile_center;
                        let segment = LineSegment::new(start, end);

                        if let Some(holes) = cell.value.holes {
                            let segments = apply_holes(holes.first_half(), segment);
                            lines.extend(segments);
                        } else {
                            lines.push(segment);
                        }
                    }

                    if is_right_solid {
                        let start = tile_center;
                        let end = tile_center + vec2(half_tile_width, 0.0);
                        let segment = LineSegment::new(start, end);

                        if let Some(holes) = cell.value.holes {
                            let segments = apply_holes(holes.second_half(), segment);
                            lines.extend(segments);
                        } else {
                            lines.push(segment);
                        }
                    }

                    if is_top_solid {
                        let start = tile_center + vec2(0.0, -half_tile_width);
                        let end = tile_center;
                        let segment = LineSegment::new(start, end);

                        if let Some(holes) = cell.value.holes {
                            let segments = apply_holes(holes.first_half(), segment);
                            lines.extend(segments);
                        } else {
                            lines.push(segment);
                        }
                    }

                    if is_bottom_solid {
                        let start = tile_center;
                        let end = tile_center + vec2(0.0, half_tile_width);
                        let segment = LineSegment::new(start, end);

                        if let Some(holes) = cell.value.holes {
                            let segments = apply_holes(holes.second_half(), segment);
                            lines.extend(segments);
                        } else {
                            lines.push(segment);
                        }
                    }
                };
            } else {
                continue;
            }
        }

        all_lines.extend(lines);
    }

    *obstacles = los::LosObstacles::with_lines(all_lines);
}

#[derive(Debug, Component)]
pub struct LosOpaqueTile;

impl Default for LosOpaqueTile {
    fn default() -> Self {
        Self
    }
}

#[derive(Debug, Default, Component)]
pub struct LosOpaqueTileExternal {
    tile_pos: IVec2,
    enabled: bool,
}

impl LosOpaqueTileExternal {
    pub fn new_from_ldtk(
        entity_instance: &EntityInstance,
        layer_instance: &bevy_ecs_ldtk::prelude::LayerInstance,
    ) -> Self {
        let tile_pos = ivec2(
            entity_instance.grid.x,
            layer_instance.c_hei - entity_instance.grid.y - 1,
        );

        Self {
            tile_pos,
            enabled: true,
        }
    }

    pub fn enable(&mut self) {
        self.enabled = true;
    }

    pub fn disable(&mut self) {
        self.enabled = false;
    }
}

fn apply_holes(holes: &[bool], segment: LineSegment) -> Vec<LineSegment> {
    let start = segment.start;
    let end = segment.end;
    let direction = (end - start).normalize();
    let length = start.distance(end);
    let step_length = length / holes.len() as f32;

    let mut result = Vec::new();

    for (i, &current_is_hole) in holes.iter().enumerate() {
        let i = i as f32;
        let current_start = start + direction * step_length * i;
        let current_end = current_start + direction * step_length;

        if !current_is_hole {
            result.push(LineSegment::new(current_start, current_end))
        }
    }

    // TODO: merge

    result
}
