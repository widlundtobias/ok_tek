use bevy::prelude::Update;
use bevy_ecs_ldtk::{
    prelude::{LdtkEntityAppExt, LdtkIntCellAppExt},
    LdtkPlugin, LevelSelection,
};

use super::{door, los::provide_los_obstacles, DoorBundle, GroundBundle, WallBundle};

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins(LdtkPlugin)
            .insert_resource(LevelSelection::Index(0))
            .register_ldtk_int_cell::<WallBundle>(1)
            .register_ldtk_int_cell::<GroundBundle>(2)
            .register_ldtk_entity::<DoorBundle>("Door")
            .add_systems(
                Update,
                (
                    provide_los_obstacles,
                    door::graphics::update_graphics,
                    door::structural::open_close,
                ),
            );
    }
}
