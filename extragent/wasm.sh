#!/bin/bash

usage () { 
echo "Usage: $0 [ -o ] [ -h ]

where:
    -h  Shows this help text.
    -o  Optimizes the wasm binary." 1>&2; 
    exit 1; 
}

OPTIMIZE_WASM=false
while getopts "ho" arg; do
    case ${arg} in 
        o) OPTIMIZE_WASM=true;;
        h) usage;; 
       \?) echo "Invalid argument passed. Check -h for valid arguments."; exit 2;;
    esac
done

mkdir -p web
cargo build --release --target=wasm32-unknown-unknown
wasm-bindgen --out-name extragent \
  --out-dir web \
  --target web target/wasm32-unknown-unknown/release/extragent.wasm
cp index.html web/
cp -r assets web

if $OPTIMIZE_WASM; then
    echo "Optimizing with wasm-opt"
    wasm-opt -O web/extragent_bg.wasm -o web/extragent_bg.wasm
fi