use bevy::{math::*, prelude::*};
use ok_math::{quads_to_mesh, LineSegment, Quad};

pub struct Level {
    walls: Vec<Quad>,
    bg: Vec<Quad>,
}

impl Default for Level {
    fn default() -> Self {
        Self {
            walls: vec![
                Quad {
                    position: vec2(60.0, 60.0),
                    size: vec2(40.0, 40.0),
                    rotation: 0.0_f32.to_radians(),
                },
                Quad {
                    position: vec2(60.0, -60.0),
                    size: vec2(40.0, 40.0),
                    rotation: 0.0_f32.to_radians(),
                },
                Quad {
                    position: vec2(-60.0, 60.0),
                    size: vec2(40.0, 40.0),
                    rotation: 0.0_f32.to_radians(),
                },
                Quad {
                    position: vec2(-60.0, -60.0),
                    size: vec2(40.0, 40.0),
                    rotation: 0.0_f32.to_radians(),
                },
                Quad {
                    position: vec2(0.0, 80.0),
                    size: vec2(40.0, 80.0),
                    rotation: 0.0_f32.to_radians(),
                },
                Quad {
                    position: vec2(0.0, 140.0),
                    size: vec2(320.0, 40.0),
                    rotation: 0.0_f32.to_radians(),
                },
                Quad {
                    position: vec2(0.0, -140.0),
                    size: vec2(320.0, 40.0),
                    rotation: 0.0_f32.to_radians(),
                },
                Quad {
                    position: vec2(140.0, 0.0),
                    size: vec2(40.0, 240.0),
                    rotation: 0.0_f32.to_radians(),
                },
                Quad {
                    position: vec2(-140.0, 80.0),
                    size: vec2(40.0, 100.0),
                    rotation: 0.0_f32.to_radians(),
                },
                Quad {
                    position: vec2(-140.0, -80.0),
                    size: vec2(40.0, 100.0),
                    rotation: 0.0_f32.to_radians(),
                },
            ],
            bg: vec![
                Quad {
                    position: vec2(-30.0, 50.0),
                    size: vec2(30.0, 180.0),
                    rotation: 79.0_f32.to_radians(),
                },
                Quad {
                    position: vec2(39.0, -1000.0),
                    size: vec2(100.0, 10.0),
                    rotation: 240.0_f32.to_radians(),
                },
                Quad {
                    position: vec2(70.0, 80.0),
                    size: vec2(40.0, 100.0),
                    rotation: 10.0_f32.to_radians(),
                },
            ],
        }
    }
}

impl From<&Level> for Mesh {
    fn from(level: &Level) -> Self {
        quads_to_mesh(&level.walls)
    }
}

impl los::ObstacleProvider for Level {
    fn line_segments(&self) -> Vec<LineSegment> {
        self.walls
            .iter()
            .cloned()
            .map(|q| q.shrunk_towards_center(5.0))
            .flat_map(|q| q.line_segments())
            .collect()
    }
}

impl Level {
    pub fn bg_mesh(&self) -> Mesh {
        quads_to_mesh(&self.bg)
    }
}
