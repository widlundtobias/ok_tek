use bevy::{prelude::*, sprite::MaterialMesh2dBundle};

mod level;

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            los::Plugin::new(los::LosConfig {
                shadow_opacity: 0.8,
                composite_camera_scale: 1.0,
            }),
        ))
        .insert_resource(ClearColor(Color::rgb(0.1, 0.1, 0.1)))
        .add_systems(Startup, setup)
        .add_systems(Update, provide_los_obstacles)
        .run();
}

const WIDTH: f32 = 1366.0;
const HEIGHT: f32 = 768.0;

fn setup(
    mut windows: Query<&mut Window>,
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let mut window = windows.single_mut();
    window.resolution.set(WIDTH, HEIGHT);

    ///////// Level setup
    let level = level::Level::default();

    // Wall mesh
    commands.spawn(MaterialMesh2dBundle {
        mesh: meshes.add(Mesh::from(&level)).into(),
        transform: Transform::default().with_translation(Vec3::new(0.0, 0.0, 1.0)),
        material: materials.add(ColorMaterial::from(Color::PURPLE)),
        ..Default::default()
    });

    // Floor decor mesh
    commands.spawn(MaterialMesh2dBundle {
        mesh: meshes.add(level.bg_mesh()).into(),
        transform: Transform::default().with_translation(Vec3::new(0.0, 0.0, 0.0)),
        material: materials.add(ColorMaterial::from(Color::DARK_GREEN)),
        ..Default::default()
    });
}

fn provide_los_obstacles(mut obstacles: ResMut<los::LosObstacles>) {
    let level = level::Level::default();

    *obstacles = los::LosObstacles::from(&level);
}
