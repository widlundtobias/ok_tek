use bevy::{
    core_pipeline::{clear_color::ClearColorConfig, core_2d::Transparent2d},
    log,
    prelude::*,
    reflect::TypeUuid,
    render::{
        render_asset::RenderAssets,
        render_phase::{AddRenderCommand, DrawFunctions, RenderPhase, SetItemPipeline},
        render_resource::{
            AsBindGroup, BindGroupLayout, BlendComponent, BlendFactor, BlendOperation, BlendState,
            PipelineCache, PreparedBindGroup, SpecializedMeshPipeline, SpecializedMeshPipelines,
        },
        renderer::RenderDevice,
        view::VisibleEntities,
        Extract, RenderApp, RenderStage,
    },
    sprite::{
        DrawMesh2d, Material2d, Material2dPlugin, MaterialMesh2dBundle, Mesh2dHandle,
        Mesh2dPipeline, Mesh2dPipelineKey, Mesh2dUniform, SetMaterial2dBindGroup,
        SetMesh2dBindGroup, SetMesh2dViewBindGroup,
    },
    utils::FloatOrd,
};

fn main() {
    let mut main_app = App::new();

    main_app
        .add_plugins(DefaultPlugins)
        .insert_resource(ClearColor(Color::rgb(1.0, 1.0, 1.0)))
        .add_startup_system(setup);

    main_app.add_plugin(Material2dPlugin::<MultiplyBlendColorMaterial>::default());

    main_app
        .world
        .resource_mut::<Assets<MultiplyBlendColorMaterial>>()
        .set_untracked(
            Handle::<MultiplyBlendColorMaterial>::default(),
            ColorMaterial {
                color: Color::rgb(1.0, 0.0, 1.0),
                ..Default::default()
            }
            .into(),
        );

    let render_app = main_app.sub_app_mut(RenderApp);

    render_app
        .add_render_command::<Transparent2d, DrawBlendMesh2d>()
        .init_resource::<MultiplyBlend2dPipeline>()
        .init_resource::<SpecializedMeshPipelines<MultiplyBlend2dPipeline>>()
        .add_system_to_stage(RenderStage::Extract, extract_multiply_blend_mesh2d)
        .add_system_to_stage(RenderStage::Queue, queue_multiply_blend_mesh2d);

    main_app.run();
}

#[derive(Clone, TypeUuid)]
#[uuid = "6d517948-329a-11ed-a261-0242ac120002"]
pub struct MultiplyBlendColorMaterial(ColorMaterial);

impl<T: Into<ColorMaterial>> From<T> for MultiplyBlendColorMaterial {
    fn from(cm: T) -> Self {
        Self(cm.into())
    };
}

impl AsBindGroup for MultiplyBlendColorMaterial {
    type Data = <ColorMaterial as AsBindGroup>::Data;

    fn as_bind_group(
        &self,
        layout: &BindGroupLayout,
        render_device: &RenderDevice,
        images: &RenderAssets<Image>,
        fallback_image: &bevy::render::texture::FallbackImage,
    ) -> Result<
        bevy::render::render_resource::PreparedBindGroup<Self>,
        bevy::render::render_resource::AsBindGroupError,
    > {
        self.0
            .as_bind_group(layout, render_device, images, fallback_image)
            .map(|bg| PreparedBindGroup {
                bindings: bg.bindings,
                bind_group: bg.bind_group,
                data: bg.data,
            })
    }

    fn bind_group_layout(render_device: &RenderDevice) -> BindGroupLayout {
        ColorMaterial::bind_group_layout(render_device)
    }
}

impl Material2d for MultiplyBlendColorMaterial {
    fn vertex_shader() -> bevy::render::render_resource::ShaderRef {
        ColorMaterial::vertex_shader()
    }

    fn fragment_shader() -> bevy::render::render_resource::ShaderRef {
        ColorMaterial::fragment_shader()
    }

    fn specialize(
        descriptor: &mut bevy::render::render_resource::RenderPipelineDescriptor,
        layout: &bevy::render::mesh::MeshVertexBufferLayout,
        key: bevy::sprite::Material2dKey<Self>,
    ) -> Result<(), bevy::render::render_resource::SpecializedMeshPipelineError> {
        log::info!("Specializing in the MultiBlendColorMaterial");

        ColorMaterial::specialize(
            descriptor,
            layout,
            bevy::sprite::Material2dKey {
                mesh_key: key.mesh_key,
                bind_group_data: key.bind_group_data,
            },
        )?;

        if let Some(fragment) = &mut descriptor.fragment {
            for target in fragment.targets.iter_mut().flatten() {
                //target.blend = Some(BlendState {
                //    color: BlendComponent {
                //        src_factor: BlendFactor::Dst,
                //        dst_factor: BlendFactor::Zero,
                //        operation: BlendOperation::Add,
                //    },
                //    alpha: BlendComponent {
                //        src_factor: BlendFactor::One,
                //        dst_factor: BlendFactor::One,
                //        operation: BlendOperation::Max,
                //    },
                //});

                //DD  cyan yellow, overlap
                //DE  blue red, overlap
                //ED  magenta everywhere
                //EE  blue red, black middle
            }
        }

        Ok(())
    }
}

// Tag entities with this to push them through the specialized pipeline for multiply blended meshes
#[derive(Component, Default)]
pub struct MultiplyBlendMesh2d;

fn setup(
    mut commands: Commands,
    mut materials: ResMut<Assets<MultiplyBlendColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    let mut cyan = Color::CYAN;
    // A cyan quad that should be rendered with multiplicative blending
    let id = commands
        .spawn_bundle(MaterialMesh2dBundle {
            mesh: meshes.add(Mesh::from(shape::Quad::default())).into(),
            transform: Transform::default()
                .with_scale(Vec3::splat(128.))
                .with_translation(Vec3::new(50.0, 50.0, 1.0)),
            material: materials.add(MultiplyBlendColorMaterial::from(cyan)),
            ..default()
        })
        .insert(MultiplyBlendMesh2d)
        .id();
    log::info!("cyan mesh id: {:?}", id);

    let mut yellow = Color::YELLOW;

    // A yellow quad that should be rendered with multiplicative blending
    let id = commands
        .spawn_bundle(MaterialMesh2dBundle {
            mesh: meshes.add(Mesh::from(shape::Quad::default())).into(),
            transform: Transform::default().with_scale(Vec3::splat(128.)),
            material: materials.add(MultiplyBlendColorMaterial::from(yellow)),
            ..default()
        })
        .insert(MultiplyBlendMesh2d)
        .id();
    log::info!("yellow mesh id: {:?}", id);

    // Main camera
    let id = commands
        .spawn_bundle(Camera2dBundle {
            camera_2d: Camera2d {
                clear_color: ClearColorConfig::Custom(Color::WHITE),
            },
            camera: Camera {
                priority: 0,
                ..Default::default()
            },
            projection: OrthographicProjection {
                ..Default::default()
            },
            ..Default::default()
        })
        .id();
    log::info!("Cam id: {:?}", id);
}

pub fn extract_multiply_blend_mesh2d(
    mut commands: Commands,
    mut previous_len: Local<usize>,
    // When extracting, you must use `Extract` to mark the `SystemParam`s
    // which should be taken from the main world.
    query: Extract<Query<(Entity, &ComputedVisibility), With<MultiplyBlendMesh2d>>>,
) {
    let mut values = Vec::with_capacity(*previous_len);
    for (entity, computed_visibility) in query.iter() {
        log::info!("In extract for entity: {:?}", entity);
        if !computed_visibility.is_visible() {
            log::info!("It was not visible!");
            continue;
        }
        log::info!("It was visible!");
        // Pass on the multiply blend tag to the extracted entity
        values.push((entity, (MultiplyBlendMesh2d,)));
    }
    *previous_len = values.len();
    commands.insert_or_spawn_batch(values);
}

/// Queue the 2d meshes marked with [`MultiplyBlendMesh2d`] using our custom pipeline and draw function
#[allow(clippy::too_many_arguments)]
pub fn queue_multiply_blend_mesh2d(
    transparent_draw_functions: Res<DrawFunctions<Transparent2d>>,
    blend_multiply_mesh2d_pipeline: Res<MultiplyBlend2dPipeline>,
    mut pipelines: ResMut<SpecializedMeshPipelines<MultiplyBlend2dPipeline>>,
    mut pipeline_cache: ResMut<PipelineCache>,
    msaa: Res<Msaa>,
    render_meshes: Res<RenderAssets<Mesh>>,
    blend_pass_mesh2d: Query<(&Mesh2dHandle, &Mesh2dUniform), With<MultiplyBlendMesh2d>>,
    mut views: Query<(&VisibleEntities, &mut RenderPhase<Transparent2d>)>,
) {
    log::info!("In the queue system");
    if blend_pass_mesh2d.is_empty() {
        log::info!("Early return on that one!");
        return;
    }

    // Iterate each view (a camera is a view)
    for (visible_entities, mut transparent_phase) in &mut views {
        log::info!("In a view");
        let draw_blend_mesh2d = transparent_draw_functions
            .read()
            .get_id::<DrawBlendMesh2d>()
            .unwrap();

        let mesh_key = Mesh2dPipelineKey::from_msaa_samples(msaa.samples);

        // Queue all entities visible to that view
        for visible_entity in &visible_entities.entities {
            log::info!("Visible entity: {:?}", visible_entity);
            if let Ok((mesh2d_handle, mesh2d_uniform)) = blend_pass_mesh2d.get(*visible_entity) {
                log::info!("Query did find mesh handle and uniform");
                // Get our specialized pipeline
                let mut mesh2d_key = mesh_key;
                if let Some(mesh) = render_meshes.get(&mesh2d_handle.0) {
                    mesh2d_key |=
                        Mesh2dPipelineKey::from_primitive_topology(mesh.primitive_topology);

                    let pipeline_id = pipelines
                        .specialize(
                            &mut pipeline_cache,
                            &blend_multiply_mesh2d_pipeline,
                            mesh2d_key,
                            &mesh.layout,
                        )
                        .unwrap();

                    let mesh_z = mesh2d_uniform.transform.w_axis.z;
                    transparent_phase.add(Transparent2d {
                        entity: *visible_entity,
                        draw_function: draw_blend_mesh2d,
                        pipeline: pipeline_id,
                        // The 2d render items are sorted according to their z value before rendering,
                        // in order to get correct transparency
                        sort_key: FloatOrd(mesh_z),
                        // This material is not batched
                        batch_range: None,
                    });
                }
            } else {
                log::info!("Query did not find anything");
            }
        }
    }
}

// This specifies how to render a blended 2d mesh
type DrawBlendMesh2d = (
    // Set the pipeline
    SetItemPipeline,
    // Set the view uniform as bind group 0
    SetMesh2dViewBindGroup<0>,
    // Set material as bind group 1
    SetMaterial2dBindGroup<MultiplyBlendColorMaterial, 1>,
    // Set the mesh uniform as bind group 2
    SetMesh2dBindGroup<2>,
    // Draw the mesh
    DrawMesh2d,
);

pub struct MultiplyBlend2dPipeline {
    mesh2d_pipeline: Mesh2dPipeline,
    pub material2d_layout: BindGroupLayout,
}

impl FromWorld for MultiplyBlend2dPipeline {
    fn from_world(world: &mut World) -> Self {
        log::info!("From world");
        let render_device = world.resource::<RenderDevice>();
        let material2d_layout = MultiplyBlendColorMaterial::bind_group_layout(render_device);
        Self {
            mesh2d_pipeline: Mesh2dPipeline::from_world(world),
            material2d_layout,
        }
    }
}

impl SpecializedMeshPipeline for MultiplyBlend2dPipeline {
    type Key = Mesh2dPipelineKey;

    fn specialize(
        &self,
        key: Self::Key,
        layout: &bevy::render::mesh::MeshVertexBufferLayout,
    ) -> Result<
        bevy::render::render_resource::RenderPipelineDescriptor,
        bevy::render::render_resource::SpecializedMeshPipelineError,
    > {
        log::info!("Specializing from existing layout: {:?}", layout);

        let mut descriptor = self.mesh2d_pipeline.specialize(key, layout)?;
        if let Some(fragment) = &mut descriptor.fragment {
            for target in fragment.targets.iter_mut().flatten() {
                //target.blend = Some(BlendState {
                //    color: BlendComponent {
                //        src_factor: BlendFactor::Dst,
                //        dst_factor: BlendFactor::Zero,
                //        operation: BlendOperation::Add,
                //    },
                //    alpha: BlendComponent {
                //        src_factor: BlendFactor::One,
                //        dst_factor: BlendFactor::One,
                //        operation: BlendOperation::Max,
                //    },
                //});
            }
        }

        descriptor.layout = Some(vec![
            self.mesh2d_pipeline.view_layout.clone(),
            self.material2d_layout.clone(),
            self.mesh2d_pipeline.mesh_layout.clone(),
        ]);

        descriptor.label = Some("specialized for multiply blending".into());

        Ok(descriptor)
    }
}
