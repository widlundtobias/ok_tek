mod config;
mod los;
mod los_entity;
mod plugin;
mod render;

pub use config::*;
pub use los::*;
pub use los_entity::*;
pub use plugin::{LosSet, Plugin, BLEND_SPRITE_MATERIAL_SHADER_HANDLE};
pub use render::{render_setup, Cameras};

use bevy::prelude::*;

#[derive(Debug, Component, Clone, Copy)]
pub struct Light2d {
    // color: white for now
}
