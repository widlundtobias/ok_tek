use bevy::prelude::Resource;

#[derive(Resource, Clone)]
pub struct LosConfig {
    pub shadow_opacity: f32,
    pub composite_camera_scale: f32,
}
