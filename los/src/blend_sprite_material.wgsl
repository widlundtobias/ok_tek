#import bevy_sprite::mesh2d_vertex_output MeshVertexOutput
    
struct BlendSpriteMaterial {
    color: vec4<f32>,
};

@group(1) @binding(0)
var<uniform> material: BlendSpriteMaterial;
@group(1) @binding(1)
var base_color_texture: texture_2d<f32>;
@group(1) @binding(2)
var base_color_sampler: sampler;

@fragment
fn fragment(
    mesh: MeshVertexOutput,
) -> @location(0) vec4<f32> {
    return material.color * textureSample(base_color_texture, base_color_sampler, mesh.uv);
}