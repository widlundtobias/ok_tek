use bevy::{math::*, prelude::*};
use ok_math::LineSegment;

pub trait ObstacleProvider {
    fn line_segments(&self) -> Vec<LineSegment>;
}

pub struct SightShape {
    pub rays: Vec<LineSegment>,
}

impl SightShape {
    pub fn calculate(start: Vec2, view_distance: f32, obstacles: &[LineSegment]) -> Self {
        // 8 rays in all directions
        let right_ray = LineSegment::new(start, start + vec2(view_distance, 0.0));
        let direction_rays = std::iter::repeat(right_ray)
            .take(8)
            .enumerate()
            .map(|(index, ray)| ray.angled_by_around_start((45.0 * index as f32).to_radians()));

        // Rays from the start towards each end of each obstacle
        let obstacle_rays = obstacles.iter().flat_map(|line| {
            let towards_start = LineSegment::new_towards(start, line.start, view_distance);
            let towards_end = LineSegment::new_towards(start, line.end, view_distance);

            let angle_offset = 0.00001;
            // Generates slightly offset rays around the given ray - one angled slightly up, the other slightly down
            let wrap_ray_with_offset_rays = |ray: LineSegment| {
                [
                    ray.angled_by_around_start(-angle_offset),
                    ray,
                    ray.angled_by_around_start(angle_offset),
                ]
            };

            let towards_start_wrapped = wrap_ray_with_offset_rays(towards_start);
            let towards_end_wrapped = wrap_ray_with_offset_rays(towards_end);

            [
                towards_start_wrapped[0],
                towards_start_wrapped[1],
                towards_start_wrapped[2],
                towards_end_wrapped[0],
                towards_end_wrapped[1],
                towards_end_wrapped[2],
            ]
        });

        let intersected_rays = direction_rays.chain(obstacle_rays).map(|mut ray| {
            //// Shorten the rays if they intersect with an obstacle
            for obstacle in obstacles {
                if let Some(intersection) = ray.intersection(obstacle) {
                    ray.end = intersection;
                }
            }

            ray
        });

        let mut rays: Vec<LineSegment> = intersected_rays.collect();

        // Sort all rays by their rotation angle
        rays.sort_by_key(|ray| ordered_float::OrderedFloat(ray.rotation()));

        Self { rays }
    }
}

impl From<&SightShape> for Mesh {
    fn from(sight_shape: &SightShape) -> Self {
        let mut indices = Vec::new();

        let mut positions = Vec::<[f32; 3]>::new();
        let mut normals = Vec::<[f32; 3]>::new();
        let mut uvs = Vec::<[f32; 2]>::new();

        if let Some(center) = sight_shape.rays.first().map(|line| line.start) {
            let mut ends: Vec<_> = sight_shape.rays.iter().map(|line| line.end).collect();
            ends.push(*ends.first().unwrap()); // Add the first point at the end to form a closed loop

            let mut index_counter = 0;

            for window in ends.windows(2) {
                // Triangle:
                let [v1, v2, v3] = [center, window[0], window[1]];

                let vertices = [
                    ([v1.x, v1.y, 0.0], [0.0, 0.0, 1.0], [0.0, 0.0]),
                    ([v2.x, v2.y, 0.0], [0.0, 0.0, 1.0], [0.0, 0.0]),
                    ([v3.x, v3.y, 0.0], [0.0, 0.0, 1.0], [0.0, 0.0]),
                ];

                let indices_array = [index_counter, index_counter + 1, index_counter + 2];
                index_counter += 3;

                indices.extend(indices_array);

                for (position, normal, uv) in vertices.iter() {
                    positions.push(*position);
                    normals.push(*normal);
                    uvs.push(*uv);
                }
            }
        }

        let indices = bevy::render::mesh::Indices::U32(indices);

        let mut mesh = Mesh::new(bevy::render::render_resource::PrimitiveTopology::TriangleList);
        mesh.set_indices(Some(indices));
        mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, positions);
        mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
        mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, uvs);
        mesh
    }
}
