use bevy::{
    asset::load_internal_asset,
    prelude::{
        apply_deferred, HandleUntyped, IntoSystemConfigs, Shader, Startup, SystemSet, Update,
    },
    reflect::TypeUuid,
    sprite::Material2dPlugin,
};

use crate::{
    render::render_setup, update_los_entity, BlendAddSpriteMaterial, BlendMultiplySpriteMaterial,
    LosConfig, LosEntity, LosObstacles,
};

pub struct Plugin {
    pub config: LosConfig,
}

impl Plugin {
    pub fn new(config: LosConfig) -> Self {
        Self { config }
    }
}

pub const BLEND_SPRITE_MATERIAL_SHADER_HANDLE: HandleUntyped =
    HandleUntyped::weak_from_u64(Shader::TYPE_UUID, 3845394853948543985);

#[derive(SystemSet, Debug, Hash, PartialEq, Eq, Clone)]
pub enum LosSet {
    SetupRendering,
    UpdateShadows,
}

impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        load_internal_asset!(
            app,
            BLEND_SPRITE_MATERIAL_SHADER_HANDLE,
            "blend_sprite_material.wgsl",
            Shader::from_wgsl
        );

        app.add_systems(
            Update,
            (update_los, update_los_entity)
                .chain()
                .in_set(LosSet::UpdateShadows),
        )
        // Apply deferred to make sure that camera resource is properly inserted before any other setup happens
        .add_systems(
            Startup,
            (render_setup, apply_deferred)
                .chain()
                .in_set(LosSet::SetupRendering),
        )
        .add_plugins((
            Material2dPlugin::<BlendAddSpriteMaterial>::default(),
            Material2dPlugin::<BlendMultiplySpriteMaterial>::default(),
        ))
        .insert_resource(self.config.clone())
        .insert_resource(LosEntity::new())
        .insert_resource(LosObstacles::new());
    }
}

fn update_los() {}
