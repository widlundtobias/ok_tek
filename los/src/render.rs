use bevy::{
    core_pipeline::clear_color::ClearColorConfig,
    prelude::*,
    render::{
        camera::RenderTarget,
        render_resource::{
            Extent3d, TextureDescriptor, TextureDimension, TextureFormat, TextureUsages,
        },
        view::RenderLayers,
    },
    sprite::MaterialMesh2dBundle,
};

use crate::{BlendMultiplySpriteMaterial, LosConfig, COMPOSITE_PASS_LAYER, SHADOW_PASS_LAYER};

#[derive(Resource)]
pub struct Cameras {
    pub world: Entity,
    pub shadows: Entity,
    pub composite: Entity,
}

const WIDTH: f32 = 1366.0;
const HEIGHT: f32 = 768.0;

pub fn render_setup(
    mut commands: Commands,
    config: Res<LosConfig>,
    mut images: ResMut<Assets<Image>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<BlendMultiplySpriteMaterial>>,
) {
    ///////// Shadow rendering setup
    let size = Extent3d {
        // TODO: I guess this should be the same size as the screen and updated to maintain it
        width: WIDTH as _,
        height: HEIGHT as _,
        ..Extent3d::default()
    };

    //// Shadow pass
    // This specifies the layer used for the shadow pass, which will be attached to the shadow pass camera.
    let shadow_pass_layer = RenderLayers::layer(SHADOW_PASS_LAYER);

    // Image/texture that the shadow pass will render to
    let mut shadow_image = Image {
        texture_descriptor: TextureDescriptor {
            label: None,
            size,
            dimension: TextureDimension::D2,
            format: TextureFormat::Bgra8UnormSrgb,
            mip_level_count: 1,
            sample_count: 1,
            usage: TextureUsages::TEXTURE_BINDING
                | TextureUsages::COPY_DST
                | TextureUsages::RENDER_ATTACHMENT,
            view_formats: &[TextureFormat::Bgra8UnormSrgb],
        },
        ..default()
    };

    // fill image.data with zeroes
    shadow_image.resize(size);

    let shadow_image_handle = images.add(shadow_image);

    // Camera for the shadow pass.
    let shadows = commands
        .spawn(Camera2dBundle {
            camera_2d: Camera2d {
                clear_color: ClearColorConfig::Custom(Color::BLACK.with_a(config.shadow_opacity)),
            },
            camera: Camera {
                // render before the composite pass camera
                order: -1,
                target: RenderTarget::Image(shadow_image_handle.clone()),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(shadow_pass_layer)
        .id();

    //// World pass
    // Image/texture that the world pass will render to
    let mut world_image = Image {
        texture_descriptor: TextureDescriptor {
            label: None,
            size,
            dimension: TextureDimension::D2,
            format: TextureFormat::Bgra8UnormSrgb,
            mip_level_count: 1,
            sample_count: 1,
            usage: TextureUsages::TEXTURE_BINDING
                | TextureUsages::COPY_DST
                | TextureUsages::RENDER_ATTACHMENT,
            view_formats: &[TextureFormat::Bgra8UnormSrgb],
        },
        ..default()
    };

    // fill image.data with zeroes
    world_image.resize(size);

    let world_image_handle = images.add(world_image);

    let world = commands
        .spawn(Camera2dBundle {
            camera: Camera {
                order: -1,
                target: RenderTarget::Image(world_image_handle.clone()),
                ..default()
            },
            projection: OrthographicProjection {
                ..Default::default()
            },
            ..Default::default()
        })
        .id();

    //// Composite pass
    let composite_pass_layer = RenderLayers::layer(COMPOSITE_PASS_LAYER);
    commands
        .spawn(MaterialMesh2dBundle {
            mesh: meshes
                .add(Mesh::from(shape::Quad {
                    size: Vec2::new(WIDTH, HEIGHT),
                    flip: false,
                }))
                .into(),
            material: materials.add(BlendMultiplySpriteMaterial {
                color: Color::WHITE,
                texture: Some(shadow_image_handle),
            }),
            ..default()
        })
        .insert(composite_pass_layer);

    commands
        .spawn(MaterialMesh2dBundle {
            mesh: meshes
                .add(Mesh::from(shape::Quad {
                    size: Vec2::new(WIDTH, HEIGHT),
                    flip: false,
                }))
                .into(),
            material: materials.add(BlendMultiplySpriteMaterial {
                color: Color::WHITE,
                texture: Some(world_image_handle),
            }),
            ..default()
        })
        .insert(composite_pass_layer);

    // Composite camera
    let composite = commands
        .spawn(Camera2dBundle {
            camera_2d: Camera2d {
                clear_color: ClearColorConfig::Custom(Color::WHITE),
            },
            camera: Camera {
                order: 0,
                ..Default::default()
            },
            projection: OrthographicProjection {
                scale: config.composite_camera_scale,
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(composite_pass_layer)
        .id();

    commands.insert_resource(Cameras {
        world,
        shadows,
        composite,
    })
}
