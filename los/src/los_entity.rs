use bevy::{
    math::{vec3, Vec3Swizzles},
    prelude::*,
    reflect::{TypePath, TypeUuid},
    render::{
        mesh::MeshVertexBufferLayout,
        render_resource::{
            AsBindGroup, BlendComponent, BlendFactor, BlendOperation, BlendState,
            RenderPipelineDescriptor, ShaderRef, SpecializedMeshPipelineError,
        },
        view::{Layer, RenderLayers},
    },
    sprite::{Material2d, Material2dKey, MaterialMesh2dBundle},
};
use ok_math::LineSegment;

use crate::{Light2d, ObstacleProvider, SightShape, BLEND_SPRITE_MATERIAL_SHADER_HANDLE};

pub const SHADOW_PASS_LAYER: Layer = 1;
pub const COMPOSITE_PASS_LAYER: Layer = 2;

#[derive(Resource)]
pub struct LosObstacles(Vec<LineSegment>);

impl<T: ObstacleProvider> From<&T> for LosObstacles {
    fn from(provider: &T) -> Self {
        Self(provider.line_segments())
    }
}

impl LosObstacles {
    pub fn new() -> Self {
        Self(Vec::new())
    }

    pub fn with_lines(lines: impl IntoIterator<Item = LineSegment>) -> Self {
        Self(lines.into_iter().collect())
    }
}

impl Default for LosObstacles {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Resource)]
pub struct LosEntity(Option<Entity>);

impl LosEntity {
    pub fn new() -> Self {
        Self(None)
    }
}

impl Default for LosEntity {
    fn default() -> Self {
        Self::new()
    }
}

const BLEND_MULTIPLY: BlendState = BlendState {
    color: BlendComponent {
        src_factor: BlendFactor::Dst,
        dst_factor: BlendFactor::OneMinusSrcAlpha,
        operation: BlendOperation::Add,
    },
    alpha: BlendComponent::OVER,
};

impl Material2d for BlendMultiplySpriteMaterial {
    fn fragment_shader() -> ShaderRef {
        BLEND_SPRITE_MATERIAL_SHADER_HANDLE.typed().into()
    }

    fn specialize(
        descriptor: &mut RenderPipelineDescriptor,
        _layout: &MeshVertexBufferLayout,
        _key: Material2dKey<Self>,
    ) -> Result<(), SpecializedMeshPipelineError> {
        if let Some(fragment) = &mut descriptor.fragment {
            if let Some(target_state) = &mut fragment.targets[0] {
                target_state.blend = Some(BLEND_MULTIPLY);
            }
        }

        Ok(())
    }
}

#[derive(AsBindGroup, TypeUuid, TypePath, Debug, Clone)]
#[uuid = "04be6ec8-e346-4e0b-8dc3-0d663c0d3182"]
pub struct BlendMultiplySpriteMaterial {
    #[uniform(0)]
    pub color: Color,
    #[texture(1)]
    #[sampler(2)]
    pub texture: Option<Handle<Image>>,
}

const BLEND_ADD: BlendState = BlendState {
    color: BlendComponent {
        src_factor: BlendFactor::SrcAlpha,
        dst_factor: BlendFactor::One,
        operation: BlendOperation::Add,
    },

    alpha: BlendComponent {
        src_factor: BlendFactor::SrcAlpha,
        dst_factor: BlendFactor::One,
        operation: BlendOperation::Add,
    },
};

impl Material2d for BlendAddSpriteMaterial {
    fn fragment_shader() -> ShaderRef {
        BLEND_SPRITE_MATERIAL_SHADER_HANDLE.typed().into()
    }

    fn specialize(
        descriptor: &mut RenderPipelineDescriptor,
        _layout: &MeshVertexBufferLayout,
        _key: Material2dKey<Self>,
    ) -> Result<(), SpecializedMeshPipelineError> {
        if let Some(fragment) = &mut descriptor.fragment {
            if let Some(target_state) = &mut fragment.targets[0] {
                target_state.blend = Some(BLEND_ADD);
            }
        }

        Ok(())
    }
}

#[derive(AsBindGroup, TypeUuid, TypePath, Debug, Clone)]
#[uuid = "a67d88f0-a69a-43ba-b4a7-00c0c9ee3332"]
pub struct BlendAddSpriteMaterial {
    #[uniform(0)]
    pub color: Color,
    #[texture(1)]
    #[sampler(2)]
    pub texture: Option<Handle<Image>>,
}

#[derive(Component)]
pub struct LosEye {}

#[allow(clippy::too_many_arguments)]
pub(crate) fn update_los_entity(
    q_camera: Query<(Option<&Camera>, &GlobalTransform, &LosEye)>,
    mut evr_cursor: EventReader<CursorMoved>,
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<BlendAddSpriteMaterial>>,
    mut los_entity: ResMut<LosEntity>,
    los_obstacles: Res<LosObstacles>,
) {
    match q_camera.get_single() {
        Ok((Some(camera), camera_transform, _)) => {
            if let Some(cursor) = evr_cursor.iter().next() {
                if let Some(origin) = camera.viewport_to_world_2d(camera_transform, cursor.position)
                {
                    let sight_shape = SightShape::calculate(origin, 1000.0, &los_obstacles.0);

                    // Despawn the existing line of sight entity
                    if let Some(existing) = los_entity.0 {
                        commands.entity(existing).despawn();
                    }

                    // TODO can we add this once?
                    let material = materials.add(BlendAddSpriteMaterial {
                        color: Color::WHITE,
                        texture: None,
                    });

                    let spawned = commands
                        .spawn(MaterialMesh2dBundle {
                            mesh: meshes.add(Mesh::from(&sight_shape)).into(),
                            transform: Transform::default().with_translation(vec3(0.0, 0.0, 0.5)),
                            material,
                            ..Default::default()
                        })
                        .insert(Light2d {})
                        .insert(RenderLayers::layer(SHADOW_PASS_LAYER))
                        .id();
                    los_entity.0 = Some(spawned);
                }
            }
        }
        Ok((None, transform, _)) => {
            let sight_shape =
                SightShape::calculate(transform.translation().xy(), 1000.0, &los_obstacles.0);

            // Despawn the existing line of sight entity
            if let Some(existing) = los_entity.0 {
                commands.entity(existing).despawn();
            }

            // TODO can we add this once?
            let material = materials.add(BlendAddSpriteMaterial {
                color: Color::WHITE,
                texture: None,
            });

            let spawned = commands
                .spawn(MaterialMesh2dBundle {
                    mesh: meshes.add(Mesh::from(&sight_shape)).into(),
                    transform: Transform::default().with_translation(vec3(0.0, 0.0, 0.5)),
                    material,
                    ..Default::default()
                })
                .insert(Light2d {})
                .insert(RenderLayers::layer(SHADOW_PASS_LAYER))
                .id();
            los_entity.0 = Some(spawned);
        }
        _ => {}
    }
}
