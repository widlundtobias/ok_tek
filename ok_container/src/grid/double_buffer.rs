use super::{Grid2d, OutOfBoundsError};

pub struct DoubleBuffer<'a, T: Clone> {
    first: &'a mut Grid2d<T>,
    second: Grid2d<T>,
}

impl<'a, T: Clone> DoubleBuffer<'a, T> {
    pub fn new(grid: &'a mut Grid2d<T>) -> Self {
        let second = grid.clone();
        Self {
            first: grid,
            second,
        }
    }

    #[inline]
    pub fn cell_count(&self) -> isize {
        self.first.cell_count()
    }

    #[inline]
    pub fn at_index(&self, index: isize) -> Result<(&T, &T), OutOfBoundsError> {
        Ok((self.first.at_index(index)?, self.second.at_index(index)?))
    }

    #[inline]
    pub fn set_index(&mut self, bottom_index: isize, val: T) -> Result<&mut T, OutOfBoundsError> {
        self.second.set_index(bottom_index, val)
    }

    #[inline]
    pub fn index_left_of(&self, index: isize) -> Result<isize, OutOfBoundsError> {
        self.first.index_left_of(index)
    }

    #[inline]
    pub fn index_right_of(&self, index: isize) -> Result<isize, OutOfBoundsError> {
        self.first.index_right_of(index)
    }

    #[inline]
    pub fn index_top_of(&self, index: isize) -> Result<isize, OutOfBoundsError> {
        self.first.index_top_of(index)
    }

    #[inline]
    pub fn index_bottom_of(&self, index: isize) -> Result<isize, OutOfBoundsError> {
        self.first.index_bottom_of(index)
    }

    pub fn finalize(self) {
        *self.first = self.second;
    }
}
