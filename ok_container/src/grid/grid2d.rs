use bevy::prelude::IVec2;
use thiserror::Error;

#[derive(Debug, Error)]
#[error("trying to access out of bounds of the grid")]
pub enum OutOfBoundsError {
    #[error("outside of width")]
    Width(i32, i32),
    #[error("outside of height")]
    Height(i32, i32),
    #[error("outside of both width and height")]
    Both((i32, i32), (i32, i32)),
    #[error("outside of the index range")]
    Index(isize, isize),
}

#[derive(Debug, Clone)]
pub struct Grid2d<T> {
    size: IVec2,
    cells: Vec<T>,
}

impl<T> Grid2d<T> {
    pub fn size(&self) -> IVec2 {
        self.size
    }

    #[inline]
    pub fn cell_count(&self) -> isize {
        self.width() as isize * self.height() as isize
    }

    #[inline]
    pub fn width(&self) -> i32 {
        self.size.x
    }

    #[inline]
    pub fn height(&self) -> i32 {
        self.size.y
    }

    #[inline]
    pub fn set(&mut self, x: i32, y: i32, val: T) -> Result<&mut T, OutOfBoundsError> {
        let index = self.cell_index(x, y)? as usize;
        self.cells[index] = val;

        Ok(&mut self.cells[index])
    }

    #[inline]
    pub fn set_index(&mut self, index: isize, val: T) -> Result<&mut T, OutOfBoundsError> {
        let cell_count = self.cell_count();
        let cell = self
            .cells
            .get_mut(index as usize)
            .ok_or(OutOfBoundsError::Index(index, cell_count))?;
        *cell = val;

        Ok(cell)
    }

    #[inline]
    pub fn get(&self, x: i32, y: i32) -> Result<&T, OutOfBoundsError> {
        let index = self.cell_index(x, y)? as usize;
        Ok(&self.cells[index])
    }

    #[inline]
    pub fn get_mut(&mut self, x: i32, y: i32) -> Result<&mut T, OutOfBoundsError> {
        let index = self.cell_index(x, y)? as usize;
        Ok(&mut self.cells[index])
    }

    #[inline]
    fn cell_index(&self, x: i32, y: i32) -> Result<isize, OutOfBoundsError> {
        let (x_valid, y_valid) = self.x_y_valid(x, y);

        match (x_valid, y_valid) {
            (true, true) => Ok((x + y * self.width()) as isize),
            (true, false) => Err(OutOfBoundsError::Height(y, self.height())),
            (false, true) => Err(OutOfBoundsError::Width(x, self.width())),
            (false, false) => Err(OutOfBoundsError::Both(
                (x, y),
                (self.width(), self.height()),
            )),
        }
    }

    #[inline]
    pub fn cell_x_y(&self, index: isize) -> Result<(i32, i32), OutOfBoundsError> {
        if index >= 0 && index < self.cell_count() {
            let w = self.width() as isize;
            Ok(((index % w) as i32, (index / w) as i32))
        } else {
            Err(OutOfBoundsError::Index(index, self.cell_count()))
        }
    }

    #[inline]
    pub fn at_index(&self, index: isize) -> Result<&T, OutOfBoundsError> {
        self.cells
            .get(index as usize)
            .ok_or_else(|| OutOfBoundsError::Index(index, self.cell_count()))
    }

    #[inline]
    pub fn get_left_of(&self, x: i32, y: i32) -> Result<Option<&T>, OutOfBoundsError> {
        self.cell_index(x, y)?;
        Ok(self.get(x - 1, y).ok())
    }

    #[inline]
    pub fn get_right_of(&self, x: i32, y: i32) -> Result<Option<&T>, OutOfBoundsError> {
        self.cell_index(x, y)?;
        Ok(self.get(x + 1, y).ok())
    }

    #[inline]
    pub fn get_top_of(&self, x: i32, y: i32) -> Result<Option<&T>, OutOfBoundsError> {
        self.cell_index(x, y)?;
        Ok(self.get(x, y - 1).ok())
    }

    #[inline]
    pub fn get_bottom_of(&self, x: i32, y: i32) -> Result<Option<&T>, OutOfBoundsError> {
        self.cell_index(x, y)?;
        Ok(self.get(x, y + 1).ok())
    }

    #[inline]
    pub fn index_left_of(&self, index: isize) -> Result<isize, OutOfBoundsError> {
        if index % self.width() as isize == 0 {
            Err(OutOfBoundsError::Index(index - 1, self.cell_count()))
        } else {
            Ok(index - 1)
        }
    }

    #[inline]
    pub fn index_right_of(&self, index: isize) -> Result<isize, OutOfBoundsError> {
        let res = index + 1;

        if res % self.width() as isize == 0 {
            Err(OutOfBoundsError::Index(res, self.cell_count()))
        } else {
            Ok(res)
        }
    }

    #[inline]
    pub fn index_top_of(&self, index: isize) -> Result<isize, OutOfBoundsError> {
        let res = index - self.width() as isize;
        if res < 0 {
            Err(OutOfBoundsError::Index(res, self.cell_count()))
        } else {
            Ok(res)
        }
    }

    #[inline]
    pub fn index_bottom_of(&self, index: isize) -> Result<isize, OutOfBoundsError> {
        let res = index + self.width() as isize;
        if res >= self.cell_count() {
            Err(OutOfBoundsError::Index(res, self.cell_count()))
        } else {
            Ok(res)
        }
    }

    pub fn iter_with_neighbors(&self) -> Grid2dIteratorWithNeighbors<'_, T> {
        Grid2dIteratorWithNeighbors {
            current_x: 0,
            current_y: 0,
            grid: self,
        }
    }

    #[inline]
    fn x_y_valid(&self, x: i32, y: i32) -> (bool, bool) {
        (x >= 0 && x < self.width(), y >= 0 && y < self.height())
    }
}

impl<T: Clone> Grid2d<T> {
    pub fn new_with_size(size: IVec2, init: T) -> Self {
        let cells = vec![init; (size.x * size.y) as usize];

        Self { size, cells }
    }
}

pub struct Grid2dIteratorWithNeighbors<'a, T> {
    current_x: i32,
    current_y: i32,
    grid: &'a Grid2d<T>,
}

impl<'a, T> Iterator for Grid2dIteratorWithNeighbors<'a, T> {
    type Item = CellWithNeighbors<'a, T>;

    fn next(&mut self) -> Option<Self::Item> {
        let next = CellWithNeighbors::at(self.current_x, self.current_y, &self.grid);

        if next.is_none() {
            return next;
        }

        if self.current_x + 1 == self.grid.width() {
            self.current_x = 0;
            self.current_y += 1;
        } else {
            self.current_x += 1;
        }

        next
    }
}

pub struct CellWithNeighbors<'a, T> {
    pub coordinate: IVec2,
    pub value: &'a T,
    pub left: Option<&'a T>,
    pub right: Option<&'a T>,
    pub top: Option<&'a T>,
    pub bottom: Option<&'a T>,
}

impl<'a, T> CellWithNeighbors<'a, T> {
    fn at(current_x: i32, current_y: i32, grid: &'a Grid2d<T>) -> Option<Self> {
        grid.get(current_x, current_y).ok().map(|value| Self {
            coordinate: IVec2::new(current_x, current_y),
            value,
            left: grid.get_left_of(current_x, current_y).ok().flatten(),
            right: grid.get_right_of(current_x, current_y).ok().flatten(),
            top: grid.get_top_of(current_x, current_y).ok().flatten(),
            bottom: grid.get_bottom_of(current_x, current_y).ok().flatten(),
        })
    }
}

#[cfg(test)]
mod tests {
    use bevy::prelude::IVec2;

    use crate::grid::Grid2d;

    use super::OutOfBoundsError;

    #[test]
    fn index_to_x_y() -> Result<(), OutOfBoundsError> {
        let grid = Grid2d::new_with_size(IVec2::new(3, 4), 0);

        assert_eq!(grid.cell_x_y(0)?, (0, 0));
        assert_eq!(grid.cell_x_y(1)?, (1, 0));
        assert_eq!(grid.cell_x_y(2)?, (2, 0));
        assert_eq!(grid.cell_x_y(3)?, (0, 1));
        assert_eq!(grid.cell_x_y(4)?, (1, 1));
        assert_eq!(grid.cell_x_y(5)?, (2, 1));
        assert_eq!(grid.cell_x_y(6)?, (0, 2));
        assert_eq!(grid.cell_x_y(7)?, (1, 2));
        assert_eq!(grid.cell_x_y(8)?, (2, 2));
        assert_eq!(grid.cell_x_y(9)?, (0, 3));
        assert_eq!(grid.cell_x_y(10)?, (1, 3));
        assert_eq!(grid.cell_x_y(11)?, (2, 3));

        Ok(())
    }

    #[test]
    fn neighboring_indices() {
        let grid = Grid2d::new_with_size(IVec2::new(5, 4), 0);

        let cell = (3, 2);
        let index = grid.cell_index(cell.0, cell.1).unwrap();

        assert_eq!(
            grid.cell_x_y(grid.index_bottom_of(index).unwrap()).unwrap(),
            (3, 3)
        );
        assert_eq!(
            grid.cell_x_y(grid.index_top_of(index).unwrap()).unwrap(),
            (3, 1)
        );
        assert_eq!(
            grid.cell_x_y(grid.index_left_of(index).unwrap()).unwrap(),
            (2, 2)
        );
        assert_eq!(
            grid.cell_x_y(grid.index_right_of(index).unwrap()).unwrap(),
            (4, 2)
        );

        let cell = (1, 1);
        let index = grid.cell_index(cell.0, cell.1).unwrap();

        assert_eq!(
            grid.index_top_of(index).unwrap(),
            index - grid.width() as isize
        );
        assert_eq!(grid.index_left_of(index).unwrap(), index - 1);

        let cell = (3, 2);
        let index = grid.cell_index(cell.0, cell.1).unwrap();

        assert_eq!(
            grid.index_bottom_of(index).unwrap(),
            index + grid.width() as isize
        );
        assert_eq!(grid.index_right_of(index).unwrap(), index + 1);

        assert!(grid.index_left_of(0).is_err());
        assert!(grid.index_left_of(5).is_err());
        assert!(grid.index_right_of(4).is_err());
        assert!(grid.index_right_of(9).is_err());

        assert!(grid.index_top_of(0).is_err());
        assert!(grid.index_top_of(4).is_err());
        assert!(grid.index_bottom_of(15).is_err());
        assert!(grid.index_bottom_of(19).is_err());
    }
}
