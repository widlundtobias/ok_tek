use bevy::prelude::*;
use thiserror::Error;

#[derive(Debug, Error)]
#[error("the given vector is zero length")]
pub struct ZeroVecError;

pub fn non_zero_vec2(v: Vec2) -> Result<Vec2, ZeroVecError> {
    if v.length() == 0.0 {
        Err(ZeroVecError)
    } else {
        Ok(v)
    }
}
