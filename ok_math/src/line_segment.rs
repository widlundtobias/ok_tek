use bevy::{
    math::{vec2, Vec2},
    prelude::*,
};

use crate::{rotate_vec, Quad, QuadPoints};

#[derive(Debug, Clone, Copy)]
pub struct LineSegment {
    pub start: Vec2,
    pub end: Vec2,
}

impl LineSegment {
    pub fn new(start: Vec2, end: Vec2) -> Self {
        Self { start, end }
    }

    pub fn new_towards(start: Vec2, towards: Vec2, length: f32) -> Self {
        let direction = (towards - start).normalize();
        let end = start + direction * length;

        Self::new(start, end)
    }

    pub fn angled_by_around_start(self, angle: f32) -> Self {
        let new_vector = rotate_vec(&(self.end - self.start), angle);
        Self::new(self.start, self.start + new_vector)
    }

    pub fn extended_by(self, length_to_add: f32) -> Self {
        let half_to_add = length_to_add / 2.0;
        let direction = self.direction();

        let new_start = self.start - direction * half_to_add;
        let new_end = self.end + direction * half_to_add;

        Self::new(new_start, new_end)
    }

    pub fn center(&self) -> Vec2 {
        (self.start + self.end) / 2.0
    }

    pub fn length(&self) -> f32 {
        (self.end - self.start).length()
    }

    pub fn direction(&self) -> Vec2 {
        (self.end - self.start).normalize()
    }

    pub fn rotation(&self) -> f32 {
        let relative = self.end - self.start;

        relative.y.atan2(relative.x)
    }

    #[allow(clippy::many_single_char_names)]
    pub fn intersection(&self, other: &LineSegment) -> Option<Vec2> {
        let p = self.start;
        let r = self.end - self.start;
        let q = other.start;
        let s = other.end - other.start;

        let cross = |v: Vec2, w: Vec2| v.x * w.y - v.y * w.x;

        let t = cross(q - p, s) / cross(r, s);

        let r_s = cross(r, s);
        let q_p_r = cross(q - p, r);

        let u = q_p_r / r_s;

        #[allow(clippy::if_same_then_else)]
        if r_s == 0.0 && q_p_r == 0.0 {
            // They are colinear, count it as no intersection
            None
        } else if r_s == 0.0 && q_p_r != 0.0 {
            // Parallel and non intersecting
            None
        } else if r_s != 0.0 && t >= 0.0 && t <= 1.0 && u >= 0.0 && u <= 1.0 {
            // They intersect
            Some(p + t * r)
        } else {
            // Not parallel but not intersecting
            None
        }
    }
}

pub fn lines_to_mesh<'a>(lines: impl IntoIterator<Item = &'a LineSegment>, width: f32) -> Mesh {
    let mut indices = Vec::new();

    let mut positions = Vec::<[f32; 3]>::new();
    let mut normals = Vec::<[f32; 3]>::new();
    let mut uvs = Vec::<[f32; 2]>::new();

    for (line_index, line) in lines.into_iter().enumerate() {
        let center = line.center();
        let length = line.length();
        let rotation = line.rotation();

        let quad = Quad {
            position: center,
            size: vec2(length, width),
            rotation,
        };

        let QuadPoints {
            north_west,
            north_east,
            south_east,
            south_west,
        } = quad.points();

        let vertices = [
            (
                [south_west.x, south_west.y, 0.0],
                [0.0, 0.0, 1.0],
                [0.0, 1.0],
            ),
            (
                [north_west.x, north_west.y, 0.0],
                [0.0, 0.0, 1.0],
                [0.0, 0.0],
            ),
            (
                [north_east.x, north_east.y, 0.0],
                [0.0, 0.0, 1.0],
                [1.0, 0.0],
            ),
            (
                [south_east.x, south_east.y, 0.0],
                [0.0, 0.0, 1.0],
                [1.0, 1.0],
            ),
        ];

        let indices_array = [0, 2, 1, 0, 3, 2];

        indices.extend(indices_array.map(|i| i + vertices.len() as u32 * line_index as u32));

        for (position, normal, uv) in vertices.iter() {
            positions.push(*position);
            normals.push(*normal);
            uvs.push(*uv);
        }
    }

    let indices = bevy::render::mesh::Indices::U32(indices);

    let mut mesh = Mesh::new(bevy::render::render_resource::PrimitiveTopology::TriangleList);
    mesh.set_indices(Some(indices));
    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, positions);
    mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
    mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, uvs);
    mesh
}
