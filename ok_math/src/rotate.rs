use bevy::math::{Mat2, Vec2};

pub fn rotate_vec(v: &Vec2, a: f32) -> Vec2 {
    let rotation_mat = Mat2::from_angle(a);
    rotation_mat * *v
}
