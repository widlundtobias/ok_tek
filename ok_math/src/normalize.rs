use super::*;
use bevy::prelude::*;

pub fn safe_normalize(v: Vec2) -> Result<Vec2, ZeroVecError> {
    non_zero_vec2(v).map(|v| v.normalize())
}
