use bevy::{math::*, prelude::*};

use crate::{rotate_vec, LineSegment};

pub struct QuadPoints {
    pub north_west: Vec2,
    pub north_east: Vec2,
    pub south_east: Vec2,
    pub south_west: Vec2,
}

#[derive(Debug, Clone)]
pub struct Quad {
    pub position: Vec2,
    pub size: Vec2,
    pub rotation: f32,
}

impl Quad {
    pub fn with_start_size(start: Vec2, size: Vec2) -> Self {
        let position = start + size / 2.0;

        Self {
            position,
            size,
            rotation: 0.0,
        }
    }

    pub fn points(&self) -> QuadPoints {
        let extent_x = self.size.x / 2.0;
        let extent_y = self.size.y / 2.0;

        let north_west = vec2(-extent_x, extent_y);
        let north_east = vec2(extent_x, extent_y);
        let south_west = vec2(-extent_x, -extent_y);
        let south_east = vec2(extent_x, -extent_y);

        let north_west = self.position + rotate_vec(&north_west, self.rotation);
        let north_east = self.position + rotate_vec(&north_east, self.rotation);
        let south_west = self.position + rotate_vec(&south_west, self.rotation);
        let south_east = self.position + rotate_vec(&south_east, self.rotation);

        QuadPoints {
            north_west,
            north_east,
            south_east,
            south_west,
        }
    }

    pub fn shrunk_towards_center(self, amount: f32) -> Self {
        let size = self.size - vec2(amount, amount);
        Self { size, ..self }
    }

    pub fn line_segments(&self) -> [LineSegment; 4] {
        let QuadPoints {
            north_west,
            north_east,
            south_east,
            south_west,
        } = self.points();

        [
            LineSegment::new(north_west, north_east).extended_by(0.001),
            LineSegment::new(north_east, south_east).extended_by(0.001),
            LineSegment::new(south_east, south_west).extended_by(0.001),
            LineSegment::new(south_west, north_west).extended_by(0.001),
        ]
    }
}

pub fn quads_to_mesh<'a>(quads: impl IntoIterator<Item = &'a Quad>) -> Mesh {
    let mut indices = Vec::new();

    let mut positions = Vec::<[f32; 3]>::new();
    let mut normals = Vec::<[f32; 3]>::new();
    let mut uvs = Vec::<[f32; 2]>::new();

    for (quad_index, quad) in quads.into_iter().enumerate() {
        let QuadPoints {
            north_west,
            north_east,
            south_east,
            south_west,
        } = quad.points();

        let vertices = [
            (
                [south_west.x, south_west.y, 0.0],
                [0.0, 0.0, 1.0],
                [0.0, 1.0],
            ),
            (
                [north_west.x, north_west.y, 0.0],
                [0.0, 0.0, 1.0],
                [0.0, 0.0],
            ),
            (
                [north_east.x, north_east.y, 0.0],
                [0.0, 0.0, 1.0],
                [1.0, 0.0],
            ),
            (
                [south_east.x, south_east.y, 0.0],
                [0.0, 0.0, 1.0],
                [1.0, 1.0],
            ),
        ];

        let indices_array = [0, 2, 1, 0, 3, 2];

        indices.extend(indices_array.map(|i| i + vertices.len() as u32 * quad_index as u32));

        for (position, normal, uv) in vertices.iter() {
            positions.push(*position);
            normals.push(*normal);
            uvs.push(*uv);
        }
    }

    let indices = bevy::render::mesh::Indices::U32(indices);

    let mut mesh = Mesh::new(bevy::render::render_resource::PrimitiveTopology::TriangleList);
    mesh.set_indices(Some(indices));
    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, positions);
    mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
    mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, uvs);
    mesh
}
