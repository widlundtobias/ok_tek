mkdir -p web
cargo build --release --target=wasm32-unknown-unknown
wasm-bindgen --out-name planto \
  --out-dir web \
  --target web target/wasm32-unknown-unknown/release/planto.wasm
cp index.html web/