#[derive(Debug, Default, Clone, Copy)]
pub struct Saturation {
    pub amount: f32,
}
