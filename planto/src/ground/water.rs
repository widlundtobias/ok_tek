use std::collections::BTreeMap;

use bevy::prelude::*;

#[derive(Debug, Resource, Default)]
pub struct WaterBodies {
    pub bodies: Vec<WaterBody>,
}

#[derive(Clone)]
pub struct WaterRun {
    pub start: i32,
    pub end: i32, // End inclusive, so a water run with 1 pixel would be same value start end
}

impl WaterRun {
    pub fn new(start: i32, end: i32) -> Self {
        debug_assert!(start <= end);
        Self { start, end }
    }

    pub fn is_overlapping(&self, other: &WaterRun) -> bool {
        !(self.end < other.start || other.end < self.start)
    }

    pub fn count(&self) -> u32 {
        (self.end + 1 - self.start) as u32
    }
}

impl std::fmt::Debug for WaterRun {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&format!("[{}..{}]", self.start, self.end))
    }
}

pub struct WaterBody {
    pub rows: BTreeMap<i32, Vec<WaterRun>>,
}

impl std::fmt::Debug for WaterBody {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let rows: Vec<_> = self.rows.iter().collect();
        f.write_str(&format!("B{{{:?}}}", rows))
    }
}

impl WaterBody {
    pub fn new_with_run(row: i32, run: WaterRun) -> Self {
        Self {
            rows: [(row, vec![run])].into_iter().collect(),
        }
    }

    pub fn is_bottom_adjacent_to(&self, row: i32, run: &WaterRun) -> bool {
        self.rows
            .iter()
            .map(|(b_row, b_runs)| {
                if b_row + 1 == row {
                    b_runs.iter().any(|body_run| body_run.is_overlapping(run))
                } else {
                    false
                }
            })
            .any(|b| b)
    }

    pub fn joined_with(mut self, subsequent: WaterBody) -> WaterBody {
        for (row, to_add) in subsequent.rows.into_iter() {
            let runs = self.rows.entry(row).or_default();
            runs.extend(to_add.into_iter());
            runs.sort_unstable_by_key(|e| e.start);
        }

        self
    }

    pub fn add_run(&mut self, row: i32, run: WaterRun) {
        let runs = self.rows.entry(row).or_default();
        runs.push(run);
    }
}
