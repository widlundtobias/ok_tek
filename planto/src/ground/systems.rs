use bevy::{log, prelude::*};

use crate::ground::WaterContainer;

use super::Ground;

#[derive(Debug, Default, Resource)]
pub struct WaterControlPanel {
    stop_water_flow: bool,
    stop_water_soak: bool,
}

pub fn update_ground_image(mut images: ResMut<Assets<Image>>, ground: Res<Ground>) {
    // let start = Instant::now();
    let image = &ground.image;
    if let Some(image) = images.get_mut(image) {
        for i in 0..ground.material.cell_count() {
            let color = ground.color_at_index(i);
            image.data[i as usize * 4] = color[0];
            image.data[i as usize * 4 + 1] = color[1];
            image.data[i as usize * 4 + 2] = color[2];
            image.data[i as usize * 4 + 3] = color[3];
        }
    }
    // let since = start.elapsed();
    // log::info!("ground took {}ms", since.as_secs_f32() * 1000.0);
}

pub fn flow_water(mut ground: ResMut<Ground>, control: Res<WaterControlPanel>) {
    if control.stop_water_flow {
        return;
    }

    // let start = Instant::now();

    ground.flow_water();

    // let since = start.elapsed();
    // log::info!("flow water bodies took {}ms", since.as_secs_f32() * 1000.0);
}

pub fn soak_water(mut ground: ResMut<Ground>, control: Res<WaterControlPanel>) {
    if control.stop_water_soak {
        return;
    }

    // let start = Instant::now();

    ground.soak_water();

    // let since = start.elapsed();
    // log::info!("soak water took {}ms", since.as_secs_f32() * 1000.0);
}
