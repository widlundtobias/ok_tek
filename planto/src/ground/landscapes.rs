pub use bevy::prelude::*;
use ok_container::grid::Grid2d;

use super::GroundMaterial;

pub fn hills(width: i32, height: i32) -> Grid2d<GroundMaterial> {
    let mut grid = Grid2d::new_with_size(IVec2::new(width, height), Default::default());

    let start_x = 150;
    let start_y = 200;

    let size = 200;

    for y in start_y..(start_y + size) {
        for x in start_x..(start_x + size) {
            let _ = grid.set(x, y, GroundMaterial::Water);
        }
    }

    let frequency = 2.0;
    let amplitude = 0.5;

    let water_level = height as f32 * 0.40;

    for x in 0..width {
        for y in 0..height {
            let x_sin = (x as f32 / 1000.0) * 2.0 * std::f32::consts::PI;
            let x_sin = x_sin * frequency;
            let surface = ((f32::sin(x_sin) * amplitude + 1.0) / 2.0) * height as f32;

            if y as f32 > surface {
                if (y as f32) < water_level {
                    // grid.set(x, y, GroundMaterial::Water).unwrap();
                } else {
                    grid.set(x, y, GroundMaterial::Soil).unwrap();
                }
            }
        }
    }

    grid
}
