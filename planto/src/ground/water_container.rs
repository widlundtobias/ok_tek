use std::collections::BTreeMap;

use rand::prelude::Distribution;

use super::{Ground, GroundMaterial, WaterBody, WaterRun};

pub trait WaterContainer {
    fn detect_water_bodies(&self) -> Vec<WaterBody>;
    fn flow_water(&mut self);
    fn soak_water(&mut self);
}

impl WaterContainer for Ground {
    fn detect_water_bodies(&self) -> Vec<WaterBody> {
        let width = self.material.size().x;

        let mut runs = BTreeMap::new();

        let mut row_tracker = 0;
        let mut row = 0;

        let mut current_water_run_start = None;

        for i in 0..self.material.cell_count() {
            let is_water = self.material.at_index(i).unwrap() == &GroundMaterial::Water;
            if is_water {
                current_water_run_start.get_or_insert(row_tracker);
            } else {
                let row_end = row_tracker - 1;
                let row_end = if row_end == -1 {
                    self.material.width() - 1
                } else {
                    row_end
                };

                let finished_run = current_water_run_start
                    .take()
                    .map(|start| WaterRun::new(start, row_end));

                if let Some(run) = finished_run {
                    let runs = runs.entry(row).or_insert_with(Vec::new);
                    runs.push(run);
                }
            }

            row_tracker += 1;
            if row_tracker == width {
                row += 1;
                row_tracker = 0;
            }
        }

        let mut finished_bodies: Vec<WaterBody> = Vec::new();
        let mut bodies_in_progress: Vec<WaterBody> = Vec::new();

        for (row, runs) in runs.into_iter() {
            for run in runs {
                let body = {
                    let bodies_touching: Vec<_> = bodies_in_progress
                        .iter()
                        .enumerate()
                        .filter_map(|(i, body)| {
                            if body.is_bottom_adjacent_to(row, &run) {
                                Some(i)
                            } else {
                                None
                            }
                        })
                        .collect();

                    let bodies_touching = {
                        let mut res = Vec::new();
                        for i in bodies_touching.into_iter().rev() {
                            res.push(bodies_in_progress.remove(i));
                        }
                        res
                    };

                    bodies_touching
                        .into_iter()
                        .reduce(|first, subsequent| first.joined_with(subsequent))
                };

                if let Some(mut body) = body {
                    body.add_run(row, run);
                    bodies_in_progress.push(body);
                } else {
                    bodies_in_progress.push(WaterBody::new_with_run(row, run));
                }
            }

            let finished_bodies_this_row: Vec<_> = bodies_in_progress
                .iter()
                .enumerate()
                .filter_map(|(i, body)| {
                    let is_finished = body.rows.last_key_value().map(|e| e.0 != &row) == Some(true);

                    if is_finished {
                        Some(i)
                    } else {
                        None
                    }
                })
                .collect();

            for i in finished_bodies_this_row.into_iter().rev() {
                let finished = bodies_in_progress.remove(i);
                finished_bodies.push(finished);
            }
        }

        finished_bodies.extend(std::mem::take(&mut bodies_in_progress));
        finished_bodies
    }

    fn flow_water(&mut self) {
        let air = &GroundMaterial::Air;

        let mut bodies = self.detect_water_bodies();

        for body in &mut bodies {
            // Detect source row
            // The initial row is the source
            if let Some((source_row, mut source_runs)) =
                body.rows.first_key_value().map(|o| (*o.0, o.1.clone()))
            {
                // Detect sinks
                let mut sinks = Vec::new();

                for (row, runs) in body.rows.iter() {
                    let is_source_row = *row == source_row;

                    let y1 = *row;
                    let y2 = y1 + 1;
                    for run in runs {
                        let x1 = run.start - 1;
                        let x2 = run.end + 1;

                        if !is_source_row {
                            // To the right/left of all rows that aren't the source row
                            let left_of = self.material.get(x1, y1).unwrap_or(air);
                            let right_of = self.material.get(x2, y1).unwrap_or(air);

                            if left_of == air {
                                sinks.push((x1, y1));
                            }
                            if right_of == air {
                                sinks.push((x2, y1));
                            }
                        }

                        // Right below the source row
                        for x in run.start..=run.end {
                            let below_of = self.material.get(x, y2).unwrap_or(air);
                            if below_of == air {
                                sinks.push((x, y2));
                            }
                        }
                    }
                }

                // The above can sometimes yield sinks twice depending
                // on the shape of the body
                sinks.sort_unstable();
                sinks.dedup();

                // Calculate flow amount
                let source_count: u32 = source_runs.iter().map(|run| run.count()).sum();
                let sink_count = sinks.len() as u32;
                let flow_amount = source_count.min(sink_count);

                // Flow source water into sinks
                for _ in 0..flow_amount {
                    let mut source_run_depleted = false;
                    if let Some(source_run) = source_runs.last_mut() {
                        let source_x = source_run.end;
                        let source_y = source_row;

                        source_run.end -= 1;

                        if source_run.count() == 0 {
                            source_run_depleted = true;
                        }

                        let chosen_sink = rand::distributions::Uniform::new(0, sinks.len())
                            .sample(&mut rand::thread_rng());

                        let (sink_x, sink_y) = sinks.remove(chosen_sink);
                        let _ = self.material.set(source_x, source_y, GroundMaterial::Air);
                        let _ = self.material.set(sink_x, sink_y, GroundMaterial::Water);
                    }

                    if source_run_depleted {
                        source_runs.pop();
                    }
                }

                // Not needed since bodies will be out of date anyway
                // body.rows.first_entry().insert(source_runs);
            }
        }

        // NOTE: water bodies are invalidated here since:
        // - new bodies might be joined together
        // - sinks have not updated the rows to include new water
    }

    fn soak_water(&mut self) {
        let soil = &GroundMaterial::Soil;
        let air = &GroundMaterial::Air;

        let bodies = self.detect_water_bodies();

        for body in bodies {
            for (row, runs) in body.rows {
                let y = row + 1;
                for run in runs {
                    for x in run.start..=run.end {
                        if self.material.get(x, y).unwrap_or(air) == soil {
                            if let Ok(saturation) = self.saturation.get_mut(x, y) {
                                let permeability = soil.permeability();
                                let _ = soil.water_capacity();

                                let rand: f32 = rand::random();
                                let will_soak = rand < permeability.0 && saturation.amount == 0.0;

                                if will_soak {
                                    self.material.set(x, row, GroundMaterial::Air).unwrap();
                                    saturation.amount = 1.0;
                                }
                            }
                        }
                    }
                }
            }
        }

        // Soak soil
    }
}
