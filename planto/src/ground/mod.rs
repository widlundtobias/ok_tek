use bevy::{
    prelude::*,
    render::render_resource::{Extent3d, TextureDimension, TextureFormat},
};
use ok_container::grid::Grid2d;

mod material;
pub use material::*;
mod saturation;
pub use saturation::*;
mod water;
pub use water::*;
mod water_container;
pub use water_container::*;
mod image;
pub use image::*;
mod systems;
pub use systems::*;
pub mod landscapes;

#[derive(Resource)]
pub struct Ground {
    pub material: Grid2d<GroundMaterial>,
    pub saturation: Grid2d<Saturation>,
    pub image: Handle<Image>,
}

impl Ground {
    pub fn color_at_index(&self, index: isize) -> [u8; 4] {
        //let mut hasher = DefaultHasher::new();
        //(x, y).hash(&mut hasher);
        let seed = index as u64 / 11; //hasher.finish();

        let material = self.material.at_index(index).copied().unwrap_or_default();
        let material_color = material.color(seed);

        if material == GroundMaterial::Soil {
            let saturation = self.saturation.at_index(index).copied().unwrap_or_default();

            let max_darken_amount = 0.4;
            let darken_amount = 1.0 - saturation.amount * max_darken_amount;
            let darken = |c: u8| (c as f32 * darken_amount) as u8;

            [
                darken(material_color[0]),
                darken(material_color[1]),
                darken(material_color[2]),
                material_color[3],
            ]
        } else {
            material_color
        }
    }

    pub fn size(&self) -> IVec2 {
        self.material.size()
    }
}

pub fn make_ground(images: &mut Assets<Image>) -> Ground {
    let ground_size = IVec2::new(1024, 1024);

    let new_image = Image::new_fill(
        Extent3d {
            width: ground_size.x as _,
            height: ground_size.x as _,
            depth_or_array_layers: 1,
        },
        TextureDimension::D2,
        &[255, 0, 255, 255],
        TextureFormat::Rgba8Unorm,
    );

    let handle = images.add(new_image);

    let material = landscapes::hills(ground_size.x, ground_size.y);
    let saturation = Grid2d::new_with_size(ground_size, Default::default());

    Ground {
        image: handle,
        material,
        saturation,
    }
}
