#[derive(Clone, Copy, Default, PartialEq, Eq)]
pub enum GroundMaterial {
    #[default]
    Air,
    Soil,
    Water,
}

impl GroundMaterial {
    pub fn color(&self, seed: u64) -> [u8; 4] {
        match self {
            GroundMaterial::Air => [0, 0, 0, 0],
            GroundMaterial::Soil => [
                [191, 144, 95, 255],
                [200, 143, 97, 255],
                [129, 96, 65, 255],
                [143, 108, 73, 255],
                [186, 139, 92, 255],
                [151, 113, 73, 255],
                [111, 84, 55, 255],
                [165, 124, 80, 255],
            ][(seed % 8) as usize],
            GroundMaterial::Water => {
                [[4, 30, 128, 255], [6, 30, 128, 255], [7, 32, 129, 255]][(seed % 3) as usize]
            }
        }
    }

    pub fn permeability(&self) -> Permeability {
        match self {
            GroundMaterial::Air => Permeability(1.0),
            GroundMaterial::Soil => Permeability(0.1),
            GroundMaterial::Water => Permeability(0.0),
        }
    }

    pub fn water_capacity(&self) -> WaterCapacity {
        match self {
            GroundMaterial::Air => WaterCapacity(0.0),
            GroundMaterial::Soil => WaterCapacity(0.5),
            GroundMaterial::Water => WaterCapacity(0.0),
        }
    }
}

/// Factor between 0..1
pub struct Permeability(pub f32);
/// In liters per pixel
pub struct WaterCapacity(f32);
