use bevy::prelude::*;

trait ImageRgba {
    fn set_pixel(&mut self, x: i32, y: i32, color: [u8; 4]);
}

impl ImageRgba for Image {
    fn set_pixel(&mut self, x: i32, y: i32, color: [u8; 4]) {
        let width = self.size().x as i32;
        let pixel_index = (x + y * width) as usize;
        self.data[pixel_index * 4] = color[0];
        self.data[pixel_index * 4 + 1] = color[1];
        self.data[pixel_index * 4 + 2] = color[2];
        self.data[pixel_index * 4 + 3] = color[3];
    }
}
