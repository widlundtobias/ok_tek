use bevy::{core_pipeline::clear_color::ClearColorConfig, prelude::*};

pub mod ground;

use ground::{flow_water, make_ground, soak_water, update_ground_image, WaterControlPanel};

fn main() {
    App::new()
        .add_plugins((DefaultPlugins,))
        .insert_resource(ClearColor(Color::rgb(0.1, 0.1, 0.1)))
        .init_resource::<WaterControlPanel>()
        .add_systems(Startup, setup)
        .add_systems(
            Update,
            (update_ground_image, (flow_water, soak_water).chain()),
        )
        .run();
}

const WIDTH: f32 = 1366.0;
const HEIGHT: f32 = 768.0;

fn setup(
    mut windows: Query<&mut Window>,
    mut images: ResMut<Assets<Image>>,
    mut commands: Commands,
) {
    let ground = make_ground(&mut images);
    let ground_image = ground.image.clone();
    commands.insert_resource(ground);

    let mut window = windows.single_mut();
    window.resolution.set(WIDTH, HEIGHT);

    ////// Camera setup
    // Camera for the world pass.
    commands.spawn(Camera2dBundle {
        camera_2d: Camera2d {
            clear_color: ClearColorConfig::Custom(Color::ALICE_BLUE),
        },
        projection: OrthographicProjection {
            ..Default::default()
        },
        ..Default::default()
    });

    // The ground sprite
    commands.spawn(SpriteBundle {
        texture: ground_image,
        ..default()
    });
}
